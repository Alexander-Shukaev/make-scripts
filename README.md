Make Scripts
============

Table of Contents
=================

-   [About](#markdown-header-about)
-   [Installation](#markdown-header-installation)
    -   [Requirements](#markdown-header-requirements)
        -   [Platforms](#markdown-header-platforms)
    -   [Instructions](#markdown-header-instructions)

About
=====

A set of robust (even paths containing spaces are fully supported) and
configurable [Make][Make/Wikipedia] scripts which constitute a convenient
front end to tedious project management manipulations over
[CMake][CMake/Wikipedia], [Git][Git/Wikipedia] (e.g. [Git Hooks][]),
[Python][Python/Wikipedia] [Virtual Environment][], and more.

Installation
============

Requirements
------------

### Platforms

-   GNU/Linux;
-   MSYS2;
-   Cygwin.

Instructions
------------

1.  Make sure that a target project is being developed with a compatible
    platform (see [Requirements](#markdown-header-requirements));

2.  In an arbitrary Git repository (of the target project), run

        :::bash
        git submodule add https://bitbucket.org/Alexander-Shukaev/make-scripts.git <make-scripts-dir>

    where, for example, the default recommended (but not enforced) value for
    `<make-scripts-dir>` is `share/make/scripts`;

3.  In order to initialize/bootstrap the Make scripts management of the target
    project, run (preferably in the root directory) either

        :::bash
        make -C <make-scripts-dir>

    for the initial (fresh) install or

        :::bash
        make -C <make-scripts-dir> clean all

    for the subsequent (re)install(s) in case something got messed up and/or a
    clean state is required;

4.  As a result, the generated `Makefile` can now be edited/customized
    according to the target project's requirements (watch out for the
    generated `TODO` hints);

5.  To learn about the default (potential) Make goals now available for the
    target project, run one of the

        :::bash
        make make.help
        make make/help

    commands;

    **NOTE:**
    > It is always possible to define additional custom Make goals and/or
    > redefine the default ones in the generated `Makefile`.

6.  Enjoy, and happy Making!  `;)`

[Make/Wikipedia]: http://en.wikipedia.org/wiki/Make_(software)

[CMake/Wikipedia]: http://en.wikipedia.org/wiki/CMake

[Git/Wikipedia]: http://en.wikipedia.org/wiki/Git

[Python/Wikipedia]: http://en.wikipedia.org/wiki/Python_(programming_language)

[Virtual Environment]: http://docs.python.org/3/tutorial/venv.html

[Git Hooks]: ../../../git-hooks
