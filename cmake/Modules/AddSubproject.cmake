### Preamble {{{
##  ==========================================================================
##        @file AddSubproject.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-10-03 Wednesday 19:50:33 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-07-11 Monday 23:53:11 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_MAKE_SCRIPTS_ADD_SUBPROJECT_CMAKE_INCLUDED)
  return()
endif()
set(_MAKE_SCRIPTS_ADD_SUBPROJECT_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(add_subproject NAME)
  set(_ADD_SUBPROJECT_MESSAGES)
  # --------------------------------------------------------------------------
  set(_ADD_SUBPROJECT_OPTIONS     REQUIRED
                                  QUIET)
  set(_ADD_SUBPROJECT_SV_KEYWORDS DIR)
  set(_ADD_SUBPROJECT_MV_KEYWORDS FIND_PACKAGE)
  cmake_parse_arguments(_ADD_SUBPROJECT_WITH
                        "${_ADD_SUBPROJECT_MV_KEYWORDS}"
                        ""
                        ""
                        ${ARGN})
  cmake_parse_arguments(_ADD_SUBPROJECT
                        ""
                        ""
                        "${_ADD_SUBPROJECT_MV_KEYWORDS}"
                        ${ARGN})
  cmake_parse_arguments(_ADD_SUBPROJECT
                        "${_ADD_SUBPROJECT_OPTIONS}"
                        "${_ADD_SUBPROJECT_SV_KEYWORDS}"
                        ""
                        ${_ADD_SUBPROJECT_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  if(NOT ${NAME}_FOUND)
    if(_ADD_SUBPROJECT_DIR)
      set(_ADD_SUBPROJECT_DIR ${_ADD_SUBPROJECT_DIR}/${NAME})
    else()
      set(_ADD_SUBPROJECT_DIR ${NAME})
    endif()
    get_filename_component(_ADD_SUBPROJECT_DIR ${_ADD_SUBPROJECT_DIR}
      ABSOLUTE)
    if(EXISTS ${_ADD_SUBPROJECT_DIR}/CMakeLists.txt)
      add_subdirectory(${_ADD_SUBPROJECT_DIR})
    else()
      set(_ADD_SUBPROJECT_MESSAGES
        "Project directory not found: ${_ADD_SUBPROJECT_DIR}"
        "\n"
        "Either run (exactly or some variation of) one of the"
        "\n"
        "  make git\n"
        "  make git.submodules\n"
        "  make git/submodules\n"
        "  git submodule update --init [--force] [--recursive] [--remote]"
        "\n"
        "commands or ensure that the required"
        "\n"
        "  ${NAME}Config.cmake\n"
        "  ${NAME}ConfigVersion.cmake"
        "\n"
        "and the optional"
        "\n"
        "  ${NAME}Targets.cmake"
        "\n"
        "files from the "
        "'https://bitbucket.org/Alexander-Shukaev/cmake-helpers.git' "
        "repository can be found by the"
        "\n"
        "  find_package"
        "\n"
        "command.")
    endif()
    # ------------------------------------------------------------------------
    if(_ADD_SUBPROJECT_MESSAGES)
      if(_ADD_SUBPROJECT_REQUIRED)
        message(FATAL_ERROR ${_ADD_SUBPROJECT_MESSAGES})
      endif()
      if(NOT _ADD_SUBPROJECT_QUIET)
        message(WARNING     ${_ADD_SUBPROJECT_MESSAGES})
      endif()
    endif()
    # ------------------------------------------------------------------------
    if(_ADD_SUBPROJECT_WITH_FIND_PACKAGE)
      find_package(${NAME} ${_ADD_SUBPROJECT_FIND_PACKAGE})
    endif()
  endif()
  # --------------------------------------------------------------------------
  unset(_ADD_SUBPROJECT_MESSAGES)
  unset(_ADD_SUBPROJECT_OPTIONS)
  unset(_ADD_SUBPROJECT_SV_KEYWORDS)
  unset(_ADD_SUBPROJECT_MV_KEYWORDS)
  unset(_ADD_SUBPROJECT_UNPARSED_ARGUMENTS)
  unset(_ADD_SUBPROJECT_REQUIRED)
  unset(_ADD_SUBPROJECT_QUIET)
  unset(_ADD_SUBPROJECT_DIR)
  unset(_ADD_SUBPROJECT_WITH_FIND_PACKAGE)
  unset(_ADD_SUBPROJECT_FIND_PACKAGE)
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
