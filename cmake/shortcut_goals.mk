### Preamble {{{
##  ==========================================================================
##        @file shortcut_goals.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-10-01 Monday 17:03:43 (+0200)
##  --------------------------------------------------------------------------
##     @created 2018-09-17 Monday 02:29:36 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS_CMAKE_SHORTCUT_GOALS_MK_INCLUDED
define _MAKE_SCRIPTS_CMAKE_SHORTCUT_GOALS_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##
ifdef _MAKE_SCRIPTS_VENV_SHORTCUT_GOALS_MK_INCLUDED
override define _MESSAGE :=
$(_MAKE_SCRIPTS_CMAKE_SHORTCUT_GOALS_MK_INCLUDED): Include not allowed:      \
conflicts with (already included) makefile                                   \
'$(_MAKE_SCRIPTS_VENV_SHORTCUT_GOALS_MK_INCLUDED)'
endef
$(error $(_MESSAGE))
override undefine _MESSAGE
endif
##
override __CMAKE_SHORTCUT_GOALS_R_FILE__ :=                                  \
  $(_MAKE_SCRIPTS_CMAKE_SHORTCUT_GOALS_MK_INCLUDED)
override __CMAKE_SHORTCUT_GOALS_R_DIR__  :=                                  \
  $(dir $(__CMAKE_SHORTCUT_GOALS_R_FILE__))
##
### Includes {{{
##  ==========================================================================
include $(__CMAKE_SHORTCUT_GOALS_R_DIR__)../.global.mk
include $(__CMAKE_SHORTCUT_GOALS_R_DIR__)../.verbose.mk
include $(__CMAKE_SHORTCUT_GOALS_R_DIR__)../cmake.mk
##  ==========================================================================
##  }}} Includes
##
override __CMAKE_SHORTCUT_GOALS_FILE__ :=                                    \
  $(abspath $(__CMAKE_SHORTCUT_GOALS_R_FILE__))
override __CMAKE_SHORTCUT_GOALS_DIR__  :=                                    \
  $(abspath $(__CMAKE_SHORTCUT_GOALS_R_DIR__))
##
ifdef VERBOSE
$(info make: Reading makefile '$(__CMAKE_SHORTCUT_GOALS_FILE__)')
endif
##
override __CMAKE_SHORTCUT_GOALS_FILE__ :=                                    \
  $(call s/\s/\\s/,$(__CMAKE_SHORTCUT_GOALS_FILE__))
override __CMAKE_SHORTCUT_GOALS_DIR__  :=                                    \
  $(call s/\s/\\s/,$(__CMAKE_SHORTCUT_GOALS_DIR__))
##
override CMAKE_DEFINE_SHORTCUT_GOALS ?= Y
override CMAKE_DEFINE_SHORTCUT_GOALS :=                                      \
  $(call uppercase,$(strip $(CMAKE_DEFINE_SHORTCUT_GOALS)))
ifeq "$(CMAKE_DEFINE_SHORTCUT_GOALS)" "N"
override CMAKE_DEFINE_SHORTCUT_GOALS :=
endif
ifeq "$(CMAKE_DEFINE_SHORTCUT_GOALS)" "0"
override CMAKE_DEFINE_SHORTCUT_GOALS :=
endif
ifdef CMAKE_DEFINE_SHORTCUT_GOALS
override CMAKE_DEFINE_SHORTCUT_GOALS := 1
endif
##
ifdef CMAKE_DEFINE_SHORTCUT_GOALS
### CMake {{{
##  ==========================================================================
### CMakeLists.txt {{{
##  ==========================================================================
.PHONY: CMakeLists.txt
CMakeLists.txt: cmake/CMakeLists.txt
##
.PHONY: !CMakeLists.txt
!CMakeLists.txt: !cmake/CMakeLists.txt
##  ==========================================================================
##  }}} CMakeLists.txt
##
### Configure {{{
##  ==========================================================================
.PHONY: configure
configure: cmake/configure
##
.PHONY: !configure
!configure: !cmake/configure
##  ==========================================================================
##  }}} Configure
##
### Build Type {{{
##  ==========================================================================
.PHONY: $(BUILD_TYPE)
$(BUILD_TYPE): cmake/$(BUILD_TYPE)
##
.PHONY: !$(BUILD_TYPE)
!$(BUILD_TYPE): !cmake/$(BUILD_TYPE)
##  ==========================================================================
##  }}} Build Type
##
### Build {{{
##  ==========================================================================
.PHONY: build
build: cmake/build
##
.PHONY: !build
!build: !cmake/build
##  ==========================================================================
##  }}} Build
##
### Builder {{{
##  ==========================================================================
.PHONY: builder%
builder%: cmake/builder%
	@
##
.PHONY: !builder%
!builder%: !cmake/builder%
	@
##
# ----------------------------------------------------------------------------
##
.PHONY: b%
b%: cmake/b%
	@
##
.PHONY: !b%
!b%: !cmake/b%
	@
##  ==========================================================================
##  }}} Builder
##
### Install {{{
##  ==========================================================================
.PHONY: install
install: cmake/install
##
.PHONY: install%
install%: cmake/install%
	@
##
.PHONY: !install
!install: !cmake/install
##
.PHONY: !install%
!install%: !cmake/install%
	@
##  ==========================================================================
##  }}} Install
##
### Installer {{{
##  ==========================================================================
.PHONY: installer%
installer%: cmake/installer%
	@
##
.PHONY: !installer%
!installer%: !cmake/installer%
	@
##
# ----------------------------------------------------------------------------
##
.PHONY: i%
i%: cmake/i%
	@
##
.PHONY: !i%
!i%: !cmake/i%
	@
##  ==========================================================================
##  }}} Installer
##
### Uninstall {{{
##  ==========================================================================
.PHONY: uninstall
uninstall: cmake/uninstall
##  ==========================================================================
##  }}} Uninstall
##
### Clean {{{
##  ==========================================================================
.PHONY: clean
clean: cmake/clean
##
.PHONY: clean%
clean%: cmake/clean%
	@
##  ==========================================================================
##  }}} Clean
##  ==========================================================================
##  }}} CMake
endif
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS_CMAKE_SHORTCUT_GOALS_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
