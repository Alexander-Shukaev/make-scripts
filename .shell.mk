### Preamble {{{
##  ==========================================================================
##        @file .shell.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-11-21 Wednesday 02:13:46 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-12-15 Tuesday 22:50:57 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS__SHELL_MK_INCLUDED
define _MAKE_SCRIPTS__SHELL_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##
### Includes {{{
##  ==========================================================================
include $(dir $(_MAKE_SCRIPTS__SHELL_MK_INCLUDED)).global.mk
include $(dir $(_MAKE_SCRIPTS__SHELL_MK_INCLUDED)).makelevel.mk
include $(dir $(_MAKE_SCRIPTS__SHELL_MK_INCLUDED)).verbose.mk
##  ==========================================================================
##  }}} Includes
##
override SHELL_VERBOSE_ORIGIN := $(origin SHELL_VERBOSE)
##
override SHELL_VERBOSE ?= N
override SHELL_VERBOSE := $(call uppercase,$(strip $(SHELL_VERBOSE)))
ifeq "$(SHELL_VERBOSE)" "N"
override SHELL_VERBOSE :=
endif
ifeq "$(SHELL_VERBOSE)" "0"
override SHELL_VERBOSE :=
endif
ifdef SHELL_VERBOSE
override SHELL_VERBOSE := 1
else
ifdef .MAKELEVEL0
ifdef VERBOSE
override define _MESSAGE :=
make: In order to print shell verbose output, re-run as the

  $(call makecmdargs,SHELL_VERBOSE=Y)

command.
endef
$(info $(_MESSAGE))
override undefine _MESSAGE
endif
endif
endif
export SHELL_VERBOSE
##
override define if_shell_verbose
$(if $(SHELL_VERBOSE),$1)
endef
##
override define if_shell_verbose_else
$(if $(SHELL_VERBOSE),$1,$2)
endef
##
override SHELL_XTRACE_ORIGIN := $(origin SHELL_XTRACE)
##
override SHELL_XTRACE ?= N
override SHELL_XTRACE := $(call uppercase,$(strip $(SHELL_XTRACE)))
ifeq "$(SHELL_XTRACE)" "N"
override SHELL_XTRACE :=
endif
ifeq "$(SHELL_XTRACE)" "0"
override SHELL_XTRACE :=
endif
ifdef SHELL_XTRACE
override SHELL_XTRACE := 1
else
ifdef .MAKELEVEL0
ifdef VERBOSE
override define _MESSAGE :=
make: In order to print shell xtrace output, re-run as the

  $(call makecmdargs,SHELL_XTRACE=Y)

command.
endef
$(info $(_MESSAGE))
override undefine _MESSAGE
endif
endif
endif
export SHELL_XTRACE
##
override define if_shell_xtrace
$(if $(SHELL_XTRACE),$1)
endef
##
override define if_shell_xtrace_else
$(if $(SHELL_XTRACE),$1,$2)
endef
##
ifndef SHELL_OPTIONS
override SHELL_OPTIONS :=
endif
ifdef SHELL_VERBOSE
override SHELL_OPTIONS += -v
endif
ifdef SHELL_XTRACE
override SHELL_OPTIONS += -x
endif
ifdef SHELL_OPTIONS
override SHELL += $(SHELL_OPTIONS)
endif
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS__SHELL_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
