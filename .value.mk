### Preamble {{{
##  ==========================================================================
##        @file .value.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-10-06 Saturday 14:38:22 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-15 Tuesday 22:50:57 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS__VALUE_MK_INCLUDED
define _MAKE_SCRIPTS__VALUE_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##
### Includes {{{
##  ==========================================================================
include $(dir $(_MAKE_SCRIPTS__VALUE_MK_INCLUDED)).global.mk
include $(dir $(_MAKE_SCRIPTS__VALUE_MK_INCLUDED)).makelevel.mk
include $(dir $(_MAKE_SCRIPTS__VALUE_MK_INCLUDED)).verbose.mk
include $(dir $(_MAKE_SCRIPTS__VALUE_MK_INCLUDED)).silent.mk
##  ==========================================================================
##  }}} Includes
##
ifndef .VALUE_VARIABLES
override .VALUE_VARIABLES :=
override .FILTER_OUT_VARIABLES += .VALUE_VARIABLES
else
$(error Defined special variable '.VALUE_VARIABLES':                         \
        reserved for internal use)
endif
##
override VALUE_ORIGIN := $(origin VALUE)
##
override VALUE ?= N
override VALUE := $(call uppercase,$(strip $(VALUE)))
ifeq "$(VALUE)" "N"
override VALUE :=
endif
ifeq "$(VALUE)" "0"
override VALUE :=
endif
ifdef VALUE
override VALUE := 1
else
ifdef .MAKELEVEL0
ifdef VERBOSE
override define _MESSAGE :=
make: In order to print value (of the '<v>' variable), re-run either as
one of the

  $(makecmdargs) .value-<v>
  $(makecmdargs) .value.<v>
  $(makecmdargs) .value/<v>

commands or to additionally print origin (of the '<v>' variable) as
one of the

  $(call makecmdargs,VERBOSE=Y) .value-<v>
  $(call makecmdargs,VERBOSE=Y) .value.<v>
  $(call makecmdargs,VERBOSE=Y) .value/<v>

commands.
make: In order to print values (of major variables), re-run either as
one of the

  $(makecmdargs) .value

commands or to additionally print origins (of major variables) as
one of the

  $(call makecmdargs,VERBOSE=Y) .value
  $(call makecmdargs,VERBOSE=Y)

commands.
make: In order to print values (of all variables), re-run either as
one of the

  $(call makecmdargs,VALUE=Y) .value
  $(call makecmdargs,VALUE=Y)

commands or to additionally print origins (of all variables) as
one of the

  $(call makecmdargs,VALUE=Y VERBOSE=Y) .value
  $(call makecmdargs,VALUE=Y VERBOSE=Y)

commands.
endef
$(info $(_MESSAGE))
override undefine _MESSAGE
endif
endif
endif
##
override define if_value
$(if $(VALUE),$1)
endef
##
override define if_value_else
$(if $(VALUE),$1,$2)
endef
##
override define .value_variables
$(sort $(call if_value_else,$(.variables),$(.VALUE_VARIABLES)))
endef
override .FILTER_OUT_VARIABLES += .value_variables
##
override define .value
$(if $1$(.value_variables),$(HRULE80)
make: Value$(if $1,,s)                                                       \
(of $(if $1,the '$1',$(call if_value_else,all,major)) variable$(if $1,,s)):
 $(foreach V,$(or $1,$(.value_variables)),$(if $(VERBOSE), [$(origin $(V))]
  , )$(V)$(if $(findstring undefined,$(flavor $(V))),,$(if                   \
              $(findstring    simple,$(flavor $(V))),:)='$(value $(V))')
)$(HRULE80))
endef
override .FILTER_OUT_VARIABLES += .value
##
### Value {{{
##  ==========================================================================
.PHONY: .value
.value: | .silent
	@$(info $(.value))
##
.PHONY: .value-%
.value-%: .value/%
	@
##
.PHONY: .value.%
.value.%: .value/%
	@
##
.PHONY: .value/%
.value/%: | .silent
	@$(info $(call .value,$*))
##  ==========================================================================
##  }}} Value
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS__VALUE_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
