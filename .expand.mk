### Preamble {{{
##  ==========================================================================
##        @file .expand.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-10-06 Saturday 14:38:22 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-15 Tuesday 22:50:57 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS__EXPAND_MK_INCLUDED
define _MAKE_SCRIPTS__EXPAND_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##
### Includes {{{
##  ==========================================================================
include $(dir $(_MAKE_SCRIPTS__EXPAND_MK_INCLUDED)).global.mk
include $(dir $(_MAKE_SCRIPTS__EXPAND_MK_INCLUDED)).makelevel.mk
include $(dir $(_MAKE_SCRIPTS__EXPAND_MK_INCLUDED)).verbose.mk
include $(dir $(_MAKE_SCRIPTS__EXPAND_MK_INCLUDED)).silent.mk
##  ==========================================================================
##  }}} Includes
##
ifndef .EXPAND_VARIABLES
override .EXPAND_VARIABLES :=
override .FILTER_OUT_VARIABLES += .EXPAND_VARIABLES
else
$(error Defined special variable '.EXPAND_VARIABLES':                        \
        reserved for internal use)
endif
##
override EXPAND_ORIGIN := $(origin EXPAND)
##
override EXPAND ?= N
override EXPAND := $(call uppercase,$(strip $(EXPAND)))
ifeq "$(EXPAND)" "N"
override EXPAND :=
endif
ifeq "$(EXPAND)" "0"
override EXPAND :=
endif
ifdef EXPAND
override EXPAND := 1
else
ifdef .MAKELEVEL0
ifdef VERBOSE
override define _MESSAGE :=
make: In order to print expansion (of the '<v>' variable), re-run either as
one of the

  $(makecmdargs) .expand-<v>
  $(makecmdargs) .expand.<v>
  $(makecmdargs) .expand/<v>
  $(makecmdargs) $$<v>

commands or to additionally print origin (of the '<v>' variable) as
one of the

  $(call makecmdargs,VERBOSE=Y) .expand-<v>
  $(call makecmdargs,VERBOSE=Y) .expand.<v>
  $(call makecmdargs,VERBOSE=Y) .expand/<v>
  $(call makecmdargs,VERBOSE=Y) $$<v>

commands.
make: In order to print expansions (of major variables), re-run either as
one of the

  $(makecmdargs) .expand
  $(makecmdargs) $$

commands or to additionally print origins (of major variables) as
one of the

  $(call makecmdargs,VERBOSE=Y) .expand
  $(call makecmdargs,VERBOSE=Y) $$
  $(call makecmdargs,VERBOSE=Y)

commands.
make: In order to print expansions (of all variables), re-run either as
one of the

  $(call makecmdargs,EXPAND=Y) .expand
  $(call makecmdargs,EXPAND=Y) $$
  $(call makecmdargs,EXPAND=Y)

commands or to additionally print origins (of all variables) as
one of the

  $(call makecmdargs,EXPAND=Y VERBOSE=Y) .expand
  $(call makecmdargs,EXPAND=Y VERBOSE=Y) $$
  $(call makecmdargs,EXPAND=Y VERBOSE=Y)

commands.
endef
$(info $(_MESSAGE))
override undefine _MESSAGE
endif
endif
endif
##
override define if_expand
$(if $(EXPAND),$1)
endef
##
override define if_expand_else
$(if $(EXPAND),$1,$2)
endef
##
override define .expand_variables
$(sort $(call if_expand_else,$(.variables),$(.EXPAND_VARIABLES)))
endef
override .FILTER_OUT_VARIABLES += .expand_variables
##
override define .expand
$(if $1$(.expand_variables),$(HRULE80)
make: Expansion$(if $1,,s)                                                   \
(of $(if $1,the '$1',$(call if_expand_else,all,major)) variable$(if $1,,s)):
 $(foreach V,$(or $1,$(.expand_variables)),$(if $(VERBOSE), [$(origin $(V))]
  , )$(V)$(if $(findstring undefined,$(flavor $(V))),,$(if                   \
              $(findstring    simple,$(flavor $(V))),:)='$($(V))')
)$(HRULE80))
endef
override .FILTER_OUT_VARIABLES += .expand
##
### Expand {{{
##  ==========================================================================
.PHONY: .expand
.expand: | .silent
	@$(info $(.expand))
##
.PHONY: .expand-%
.expand-%: .expand/%
	@
##
.PHONY: .expand.%
.expand.%: .expand/%
	@
##
.PHONY: .expand/%
.expand/%: | .silent
	@$(info $(call .expand,$*))
##  ==========================================================================
##  }}} Expand
##
### $ {{{
##  ==========================================================================
.PHONY: $$
$$: .expand
##
.PHONY: $$%
$$%: .expand/%
	@
##  ==========================================================================
##  }}} $
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS__EXPAND_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
