#!/usr/bin/env python
# encoding: utf-8
##
### Preamble {{{
##  ==========================================================================
##        @file __init__.py
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-10-04 Thursday 00:55:32 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-01-05 Tuesday 22:02:47 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Imports {{{
##  ==========================================================================
from __future__ import absolute_import
from __future__ import print_function
##
from os.path import basename
from os.path import dirname
from os.path import isdir
from os.path import join
from os.path import splitext
##
import errno
import io
import logging
import re
import sys
##  ==========================================================================
##  }}} Imports
##
### Constants {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
__all__ = [
  'find',
  'find_packages',
  'find_py_modules',
  'find_data_files',
  'read',
  'parse_readme',
  'parse_changelog',
  'parse_long_description',
  'parse_requirements',
  'parse_freeze',
  'check_requirements',
  'main',
]
##  ==========================================================================
##  }}} Public
##
### Private {{{
##  ==========================================================================
##  ==========================================================================
##  }}} Private
##  ==========================================================================
##  }}} Constants
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def find(*paths, **kwargs):
  from glob import glob
  return glob(join(kwargs.get('where') or '.', *paths))
##
def find_packages(*args, **kwargs):
  try:
    from setuptools     import find_packages
  except ImportError:
    from distutils.core import find_packages
  return find_packages(*args, **kwargs)
##
def find_py_modules(where='.', **kwargs):
  return [splitext(basename(path))[0]
          for path in find('*.py', where=(where or '.'))]
##
def find_data_files(where='.', **kwargs):
  from os import walk
  return [(d, [join(d, f) for f in files])
          for d, _, files in walk(where or '.')]
##
def read(*paths, **kwargs):
  if not paths or not ''.join(paths):
    raise ValueError("'paths' must be joinable into a non-empty string")
  try:
    return io.open(join(kwargs.get('where') or '.', *paths),
                   encoding=(kwargs.get('encoding') or 'utf8')).read()
  except IOError:
    return ''
##
def parse_readme(*paths, **kwargs):
  text      = read(*paths, **kwargs)
  extension = extension_lower(paths[-1])
  if extension == 'rst':
    return re.compile(r'^.. start-badges.*^.. end-badges',
                      re.M | re.S).sub('', text),
  if extension == 'md':
    return re.sub(r'(?m)^```.*[\r]?[\n]?', '', text)
  if extension == 'txt' or not extension:
    return text
  raise ValueError("Paths to README file 'paths' (which are ${0}) "
                   "have unknown extension '${1}'".format(paths, extension))
##
def parse_changelog(*paths, **kwargs):
  text      = read(*paths, **kwargs)
  extension = extension_lower(paths[-1])
  if extension == 'rst':
    return re.sub(r':[a-z]+:`~?(.*?)`', r'``\1``', text)
  if extension == 'md':
    return re.sub(r'(?m)^```.*[\r]?[\n]?', '', text)
  if extension == 'txt' or not extension:
    return text
  raise ValueError("Paths to CHANGELOG file 'paths' (which are ${0}) "
                   "have unknown extension '${1}'".format(paths, extension))
##
def parse_long_description(**kwargs):
  def parse(name, parser, **kwargs):
    descriptions = []
    paths        = kwargs.get(name.lower() + '_paths')
    kwargs       = dict(
      where=kwargs.get(name.lower() + '_where', kwargs.get('where')),
      encoding=kwargs.get(name.lower() + '_encoding', kwargs.get('encoding')),
    )
    if paths is None:
      for extension in ('.rst', '.md', '.txt', ''):
        description = parser(name.upper() + extension, **kwargs)
        if description:
          descriptions.extend(description)
    else:
      paths       = toiter(paths)
      description = parser(*paths, **kwargs)
      if description:
        descriptions.extend(description)
    return descriptions
  ##
  return '\n'.join(parse('README',    parse_readme,    **kwargs) +
                   parse('CHANGELOG', parse_changelog, **kwargs))
##
def parse_requirements(*paths, **kwargs):
  if not paths:
    paths = toiter('requirements.txt')
  strings         = read(*paths, **kwargs).splitlines()
  kwargs['where'] = dirname(join(kwargs.get('where') or '.', *paths))
  lines = iter(enumerate(strings, 1))
  for number, string in lines:
    string = string.strip()
    if not string or string.startswith(('-c', '-e', '-f', '-i', '--', '#')):
      continue
    match = re.search(r'[^\S\n\r]+\#', string)
    if match:
      string = string[:match.start()].rstrip()
    if string.endswith('\\'):
      string = string[:-2].rstrip() + next(lines, (number + 1, ''))[1]
    if string.startswith(('-r')):
      string = dequote(string[2:].lstrip())
      for string in parse_requirements(string, **kwargs):
        yield string
    else:
      match = re.search(r'[^\S\n\r]+\-[-cefi]', string)
      if match:
        string = dequote(''.join(string[:match.start()].split()))
      try:
        from pkg_resources import Requirement
        requirement = Requirement(string)
        from pkg_resources import safe_name
        requirement.name = safe_name(requirement.name)
        string = str(requirement)
      except ImportError:
        pass
      if kwargs.get('loose', not {'develop'}.intersection(sys.argv)):
        yield string.replace('==', '>=')
      else:
        yield string
##
def parse_freeze(*paths, **kwargs):
  if not paths:
    paths = toiter('freeze.txt')
  return parse_requirements(*paths, **kwargs)
##
def check_requirements(requirements=None, freeze=None):
  if requirements is None:
    requirements = parse_requirements()
  if freeze       is None:
    freeze       = parse_freeze()
  requirements = set(requirements)
  freeze       = set(freeze)
  l            = Log()
  if   requirements.issubset(freeze):
    if l.isEnabledFor(logging.INFO):
      l.info("'requirements' (which is %s) is a subset of "
             "'freeze' (which is %s)", requirements or '{}', freeze or '{}')
  elif freeze.issubset(requirements):
    if l.isEnabledFor(logging.INFO):
      l.info("'requirements' (which is %s) is a superset of "
             "'freeze' (which is %s)", requirements or '{}', freeze or '{}')
  else:
    e = ValueError("'requirements' (which is {0}) "
                   "is neither a subset nor a superset of "
                   "'freeze' (which is {1})".format(requirements or '{}',
                                                    freeze       or '{}'))
    if l.isEnabledFor(logging.ERROR):
      l.error(e)
    raise e
  return list(requirements)
##
def main(**attrs):
  name = attrs.get('name')
  if not name or not isstr(name):
    l = Log()
    e = ValueError("'name' must be a non-empty string"
                   ", not '{0}'".format(type(name).__name__))
    if l.isEnabledFor(logging.ERROR):
      l.error(e)
    raise e
  use_scm_version = attrs.get('use_scm_version')
  if use_scm_version:
    write_to = use_scm_version.get('write_to')
    if write_to:
      makedirs(dirname(write_to))
  from os import devnull
  try:
    from setuptools     import setup
    opt = '--single-version-externally-managed'
    if not opt in sys.argv:
      try:
        sys.argv.insert(sys.argv.index('install') + 1, opt)
      except ValueError:
        pass
    opt = '--record='
    if not [arg for arg in sys.argv if arg.startswith(opt)]:
      try:
        sys.argv.insert(sys.argv.index('install') + 1, opt + devnull)
      except ValueError:
        pass
  except ImportError:
    from distutils.core import setup
  setup(**attrs)
  return 0
##  ==========================================================================
##  }}} Public
##
### Private {{{
##  ==========================================================================
def basename_and_extension(p):
  return basename(p).rsplit('.', 1)
##
def basename_sans_extension(p):
  return basename_and_extension(p)[0]
##
def dequote(s):
  if len(s) > 1 and s[0] == s[-1] and s.startswith(("'", '"')):
      return s[1:-1]
  return s
##
def extension(p):
  return (basename_and_extension(p)[1:2] or [None])[0]
##
def extension_lower(p):
  return splitext(basename(p))[1][1:].lower()
##
def isstr(o):
  try:
    basestring
  except NameError:
    basestring = (str, bytes)
  return isinstance(o, basestring)
##
def makedirs(path):
  from os import makedirs
  try:
    makedirs(path)
  except OSError as e:
    if e.errno != errno.EEXIST or not isdir(path):
      raise
##
def toiter(o):
  if not isstr(o):
    try:
      return iter(o)
    except TypeError:
      pass
  return iter([o])
##
def tolist(o):
  if not isstr(o):
    try:
      return list(o)
    except TypeError:
      pass
  return [o]
##  ==========================================================================
##  }}} Private
##  ==========================================================================
##  }}} Functions
##
### Classes {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
##  ==========================================================================
##  }}} Public
##
### Private {{{
##  ==========================================================================
class Log:
  __l = None
  ##
  def __init__(self):
    assert (bool(Log.__l) ==
            bool(__name__ in logging.Logger.manager.loggerDict))
    if Log.__l:
      return
    logging.basicConfig(format='%(levelname)s: %(message)s',
                        level=logging.DEBUG)
    Log.__l = logging.getLogger(__name__)
  ##
  def __getattr__(self, name):
    return getattr(self.__l, name)
  ##
  @property
  def logger(self):
    return self.__l
##  ==========================================================================
##  }}} Private
##  ==========================================================================
##  }}} Classes
