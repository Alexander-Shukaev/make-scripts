### Preamble {{{
##  ==========================================================================
##        @file git.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-09-25 Tuesday 22:34:38 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-15 Tuesday 21:03:28 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS_GIT_MK_INCLUDED
define _MAKE_SCRIPTS_GIT_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##
### Includes {{{
##  ==========================================================================
include $(dir $(_MAKE_SCRIPTS_GIT_MK_INCLUDED))git/Makefile
##  ==========================================================================
##  }}} Includes
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS_GIT_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
