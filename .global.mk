### Preamble {{{
##  ==========================================================================
##        @file .global.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-11-21 Wednesday 02:36:12 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-12-15 Tuesday 22:50:57 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS__GLOBAL_MK_INCLUDED
define _MAKE_SCRIPTS__GLOBAL_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##
.NOTPARALLEL:
##
.SECONDEXPANSION:
##
.SUFFIXES:
##
override SUFFIXES :=
##
override .FILTER_OUT_VARIABLES := .FILTER_OUT_VARIABLES .VARIABLES
##
# ----------------------------------------------------------------------------
override define $$ :=
$$
endef
override define \% :=
%
endef
override define \+ :=
+
endef
override define \, :=
,
endef
override define \- :=
-
endef
override define \; :=
;
endef
override define \c :=
:
endef
override define \e :=
=
endef
override define \h :=
\#
endef
override define \n :=

$()
endef
override define \s :=
$() $()
endef
override define \t :=
$()	$()
endef
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
# NOTE:
#
# The following statement from the GNU Make Manual is either false or
# ill-formed:
#
# > A variable name may be any sequence of characters not containing ':', '#',
# > '=', or whitespace.  [1]
#
override define \$(\c) :=
$(\c)
endef
override define \$(\e) :=
$(\e)
endef
override define \$(\h) :=
$(\h)
endef
override define \$(\s) :=
$(\s)
endef
override define \$(\t) :=
$(\t)
endef
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
# NOTE:
#
# For (long) line continuation/splitting, the following statement from the GNU
# Make Manual is useful:
#
# > Outside of recipe lines, backslash/newlines are converted into a single
# > space character.  [2]
#
# That is a combination of dollar/backslash/newline should syntactically
# result in an expansion of the variable with the name consisting of a single
# space character ' ', which if either undefined or defined as an empty
# string, should expand to an empty string and serve well as a true (long)
# line continuation solution:
#
ifndef $(\s)
override $(\s) :=
override .FILTER_OUT_VARIABLES += $(\s)
else
$(error Defined special variable '$(\s)': reserved for internal use)
endif
ifndef $(\t)
override $(\t) :=
override .FILTER_OUT_VARIABLES += $(\t)
else
$(error Defined special variable '$(\t)': reserved for internal use)
endif
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
override define HRULE1  :=
$(\-)
endef
override .FILTER_OUT_VARIABLES += HRULE1
override define HRULE2  :=
$(HRULE1)$(HRULE1)
endef
override .FILTER_OUT_VARIABLES += HRULE2
override define HRULE4  :=
$(HRULE2)$(HRULE2)
endef
override .FILTER_OUT_VARIABLES += HRULE4
override define HRULE8  :=
$(HRULE4)$(HRULE4)
endef
override .FILTER_OUT_VARIABLES += HRULE8
override define HRULE16 :=
$(HRULE8)$(HRULE8)
endef
override .FILTER_OUT_VARIABLES += HRULE16
override define HRULE32 :=
$(HRULE16)$(HRULE16)
endef
override .FILTER_OUT_VARIABLES += HRULE32
override define HRULE64 :=
$(HRULE32)$(HRULE32)
endef
override .FILTER_OUT_VARIABLES += HRULE64
override define HRULE80 :=
$(HRULE64)$(HRULE16)
endef
override .FILTER_OUT_VARIABLES += HRULE80
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
ifndef &
override define &
$(firstword $|)
endef
override .FILTER_OUT_VARIABLES += &
else
$(error Defined special variable '&': reserved for internal use)
endif
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
override define s/\s/%20/
$(subst $(\s),%20,$1)
endef
##
override define s/%20/\s/
$(subst %20,$(\s),$1)
endef
##
override define s/\s/\c/
$(subst $(\s),$(\c),$1)
endef
##
override define s/\c/\s/
$(subst $(\c),$(\s),$1)
endef
##
override define s/\s/\n/
$(subst $(\s),$(\n),$1)
endef
##
override define s/\n/\s/
$(subst $(\n),$(\s),$1)
endef
##
override define s/\s/\\s/
$(subst $(\s),\$(\s),$1)
endef
##
override define s/\\s/\s/
$(subst \$(\s),$(\s),$1)
endef
##
override define s/\s/\\c/
$(subst $(\s),\$(\c),$1)
endef
##
override define s/\\c/\s/
$(subst \$(\c),$(\s),$1)
endef
##
override define s/\\s/\\c/
$(subst \$(\s),\$(\c),$1)
endef
##
override define s/\\c/\\s/
$(subst \$(\c),\$(\s),$1)
endef
##
override define s/\n/\c/
$(subst $(\n),$(\c),$1)
endef
##
override define s/\c/\n/
$(subst $(\c),$(\n),$1)
endef
##
override define s/\n/\\n/
$(subst $(\n),\$(\n),$1)
endef
##
override define s/\\n/\n/
$(subst \$(\n),$(\n),$1)
endef
##
override define s/\n/\\c/
$(subst $(\n),\$(\c),$1)
endef
##
override define s/\\c/\n/
$(subst \$(\c),$(\n),$1)
endef
##
override define s/\\n/\\c/
$(subst \$(\n),\$(\c),$1)
endef
##
override define s/\\c/\\n/
$(subst \$(\c),\$(\n),$1)
endef
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
override define makecmdargs
$(MAKE)$\
$(if $(MAKEFLAGS), -$(MAKEFLAGS))$\
$(if $(-*-command-variables-*-), $(-*-command-variables-*-))$\
$(if $1, $1)$\
$(if $(MAKECMDGOALS), $(MAKECMDGOALS))
endef
##
override define filename
$(call s/\\c/\s/,$(notdir $(patsubst %/,%,$(call s/\s/\\c/,$1))))
endef
##
override define dirname
$(patsubst %/,%,$(dir $(patsubst %/,%,$1)))
endef
##
override define find
$(if $1,$(call s/\\c/\s/,$(call s/\s/\\s/,$(patsubst %\:,%,$\
$(shell find $1 -printf '%p\\:' 2> '/dev/null')))))
endef
##
override define glob
$(if $1,$(call s/\\c/\s/,$(call s/\s/\\s/,$(patsubst %\:,%,$\
$(shell printf '%s\:' $1 2> '/dev/null')))))
endef
##
override define lowercase
$(subst A,a,$\
$(subst B,b,$\
$(subst C,c,$\
$(subst D,d,$\
$(subst E,e,$\
$(subst F,f,$\
$(subst G,g,$\
$(subst H,h,$\
$(subst I,i,$\
$(subst J,j,$\
$(subst K,k,$\
$(subst L,l,$\
$(subst M,m,$\
$(subst N,n,$\
$(subst O,o,$\
$(subst P,p,$\
$(subst Q,q,$\
$(subst R,r,$\
$(subst S,s,$\
$(subst T,t,$\
$(subst U,u,$\
$(subst V,v,$\
$(subst W,w,$\
$(subst X,x,$\
$(subst Y,y,$\
$(subst Z,z,$\
$1))))))))))))))))))))))))))
endef
##
override define uppercase
$(subst a,A,$\
$(subst b,B,$\
$(subst c,C,$\
$(subst d,D,$\
$(subst e,E,$\
$(subst f,F,$\
$(subst g,G,$\
$(subst h,H,$\
$(subst i,I,$\
$(subst j,J,$\
$(subst k,K,$\
$(subst l,L,$\
$(subst m,M,$\
$(subst n,N,$\
$(subst o,O,$\
$(subst p,P,$\
$(subst q,Q,$\
$(subst r,R,$\
$(subst s,S,$\
$(subst t,T,$\
$(subst u,U,$\
$(subst v,V,$\
$(subst w,W,$\
$(subst x,X,$\
$(subst y,Y,$\
$(subst z,Z,$\
$1))))))))))))))))))))))))))
endef
##
override define prefix_1
$(if $(or $\
$(patsubst $(abspath $3)%,,$(abspath $1)),$\
$(patsubst $(abspath $3)%,,$(abspath $2))),$\
$(strip $(call prefix_1,$1,$2,$(call dirname,$3))),$\
$(strip $(abspath $3)))
endef
##
override define prefix
$(call prefix_1,$1,$2,$1)
endef
##
override define relpath_1
$(patsubst /%,%,$(subst $(\s),/,$(patsubst %,..,$(subst /,$(\s),$\
$(patsubst $3%,%,$(abspath $2)))))$\
$(patsubst $3%,%,$(abspath $1)))
endef
##
override define relpath
$(call relpath_1,$1,$2,$(call prefix,$1,$2))
endef
##
override define expand
$(if $(findstring simple,$(flavor $1)),$(subst $$,$$$$,$($1)),$(value $1))
endef
##
override define append
$(if $(findstring simple,$(flavor $1)),$\
$(eval override $1 := $(call expand,$1) $2),$\
$(eval override $1  = $(call expand,$1) $2))
endef
##
override define prepend
$(if $(findstring simple,$(flavor $1)),$\
$(eval override $1 := $2 $(call expand,$1)),$\
$(eval override $1  = $2 $(call expand,$1)))
endef
##
override define make_recursive
$(if $(findstring simple,$(flavor $1)),$\
$(eval override $1  = $(call expand,$1)))
endef
##
override define make_simple
$(if $(findstring recursive,$(flavor $1)),$\
$(eval override $1 := $(call expand,$1)))
endef
##
override define exist_only
$(if $1,$(if $(wildcard $1),,$1))
endef
##
override define findstring_maybe
$(if $1,$(findstring $3$1$4,$2))
endef
##
override define uniq
$(if $1,$(firstword $1) $(call uniq,$(filter-out $(firstword $1),$1)))
endef
##
override define version
$(if $1,$(lastword $\
$(shell $1 $(if $2,$2,--version) 2> '/dev/null' | head -n 1)))
endef
##
override define which
$(if $1,$(shell which $1 2> '/dev/null'))
endef
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
override define !if
$(if $1,$1,$(if $4,$\
$(warning empty string$(if $2, in$(if $3, $3 of) variable '$2')),$\
$(error   empty string$(if $2, in$(if $3, $3 of) variable '$2'))))
endef
override .FILTER_OUT_VARIABLES += !if
##
override define !expand
$(if $1,$(call !if,$($1),$1,expansion,$2),$(if $2,$\
$(warning empty variable name),$\
$(error   empty variable name)))
endef
override .FILTER_OUT_VARIABLES += !expand
##
override define !value
$(if $1,$(call !if,$(value $1),$1,value,$2),$(if $2,$\
$(warning empty variable name),$\
$(error   empty variable name)))
endef
override .FILTER_OUT_VARIABLES += !value
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
override define !exist
$(if $1,$1,$(if $4,$(warning                                                 \
  $(if $2,$(call s/\\s/\s/,$($2)): No such file or directory:)               \
  empty string$(if $2, in$(if $3, $3 of) variable '$2')),$(error             \
  $(if $2,$(call s/\\s/\s/,$($2)): No such file or directory:)               \
  empty string$(if $2, in$(if $3, $3 of) variable '$2'))))
endef
override .FILTER_OUT_VARIABLES += !exist
##
override define !wildcard
$(if $1,$(call !exist,$(wildcard $($1)),$1,wildcard,$2),$(if $2,$(warning    \
  empty variable name),$(error                                               \
  empty variable name)))
endef
override .FILTER_OUT_VARIABLES += !wildcard
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
override define .variables
$(filter-out $(.FILTER_OUT_VARIABLES),$(.VARIABLES))
endef
override .FILTER_OUT_VARIABLES += .variables
# ----------------------------------------------------------------------------
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS__GLOBAL_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
##
### References {{{
##  ==========================================================================
##  [1] Info node `(make) Using Variables'
##  [2] Info node `(make) Splitting Lines'
##  ==========================================================================
##  }}} References
