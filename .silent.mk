### Preamble {{{
##  ==========================================================================
##        @file .silent.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-10-06 Saturday 14:36:51 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-15 Tuesday 22:50:57 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS__SILENT_MK_INCLUDED
define _MAKE_SCRIPTS__SILENT_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##
### Includes {{{
##  ==========================================================================
include $(dir $(_MAKE_SCRIPTS__SILENT_MK_INCLUDED)).global.mk
include $(dir $(_MAKE_SCRIPTS__SILENT_MK_INCLUDED)).makelevel.mk
include $(dir $(_MAKE_SCRIPTS__SILENT_MK_INCLUDED)).verbose.mk
##  ==========================================================================
##  }}} Includes
##
override SILENT_ORIGIN := $(origin SILENT)
##
override SILENT ?= $(if $(VERBOSE),N,Y)
override SILENT := $(call uppercase,$(strip $(SILENT)))
ifeq "$(SILENT)" "N"
override SILENT :=
endif
ifeq "$(SILENT)" "0"
override SILENT :=
endif
ifdef SILENT
override SILENT := 1
else
ifdef .MAKELEVEL0
ifdef VERBOSE
override define _MESSAGE :=
make: In order to silence recipe output, re-run either as the

  $(call makecmdargs,SILENT=Y)

command or as the

  $(call makecmdargs,VERBOSE=N)

command.
endef
$(info $(_MESSAGE))
override undefine _MESSAGE
endif
endif
endif
export SILENT
##
override define if_silent
$(if $(SILENT),$1)
endef
##
override define if_silent_else
$(if $(SILENT),$1,$2)
endef
##
### Silent {{{
##  ==========================================================================
ifdef SILENT
# ----------------------------------------------------------------------------
# NOTE:
#
.SILENT:
#
# Should deliver the same behavior as:
#
#   MAKEFLAGS += --silent
# ----------------------------------------------------------------------------
endif
##
.PHONY: .silent
.silent:
	@:
##  ==========================================================================
##  }}} Silent
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS__SILENT_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
