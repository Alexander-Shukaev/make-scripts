### Preamble {{{
##  ==========================================================================
##        @file make.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-09-25 Tuesday 22:33:36 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-16 Wednesday 20:17:26 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS_MAKE_MK_INCLUDED
define _MAKE_SCRIPTS_MAKE_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
### Includes {{{
##  ==========================================================================
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##  ==========================================================================
##  }}} Includes
##
include $(dir $(_MAKE_SCRIPTS_MAKE_MK_INCLUDED))git.mk
include $(dir $(_MAKE_SCRIPTS_MAKE_MK_INCLUDED))make/Makefile
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS_MAKE_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
