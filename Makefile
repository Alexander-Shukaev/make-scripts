### Preamble {{{
##  ==========================================================================
##        @file Makefile
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2020-12-19 Saturday 14:26:07 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-12-16 Wednesday 18:48:46 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2020,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
ifeq "$(PWD)" "$(CURDIR)"
### Header {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
include header.mk
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Header
##
### Body {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
include git.mk
##  ==========================================================================
##  }}} Includes
##
### Variables {{{
##  ==========================================================================
# Default
override .DEFAULT_GOAL := all
##  ==========================================================================
##  }}} Variables
##
### Targets {{{
##  ==========================================================================
### All {{{
##  ==========================================================================
.PHONY: all
all: git
##  ==========================================================================
##  }}} All
##  ==========================================================================
##  }}} Targets
##  ==========================================================================
##  }}} Body
##
### Footer {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
include footer.mk
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Footer
else
override __R_FILE__ := $(lastword $(MAKEFILE_LIST))
override __R_DIR__  := $(dir      $(__R_FILE__))
##
### Includes {{{
##  ==========================================================================
include $(__R_DIR__)header.mk
##
include $(__R_DIR__).global.mk
include $(__R_DIR__).makelevel.mk
include $(__R_DIR__).verbose.mk
include $(__R_DIR__).silent.mk
##  ==========================================================================
##  }}} Includes
##
override __CURDIR__ := $(call s/\s/\\c/,$(CURDIR))
override __PWD__    := $(call s/\s/\\c/,$(PWD))
##
ifndef PREFIX
override PREFIX      := $(call relpath,$(__CURDIR__),$(__PWD__))/
override PREFIX      := $(call s/\\c/\s/,$(PREFIX))
else
override PREFIX      := $(call s/\\s/\s/,$(PREFIX))
endif
ifneq "$(PREFIX)" "$(firstword $(PREFIX))"
$(error $(PREFIX): Prefix not allowed: contains whitespace)
endif
ifndef PREFIX_MK
override PREFIX_MK   := $(call s/\\c/\s/,$(__PWD__))/PREFIX.mk
else
override PREFIX_MK   := $(call s/\\s/\s/,$(PREFIX_MK))
endif
ifndef LICENSE_TXT
override LICENSE_TXT := $(call s/\\c/\s/,$(__PWD__))/LICENSE.txt
else
override LICENSE_TXT := $(call s/\\s/\s/,$(LICENSE_TXT))
endif
ifndef MAKEFILE
override MAKEFILE    := $(call s/\\c/\s/,$(__PWD__))/Makefile
else
override MAKEFILE    := $(call s/\\s/\s/,$(MAKEFILE))
endif
ifndef MAKE_DIR
override MAKE_DIR    := $(call s/\\c/\s/,$(__PWD__))/.make
else
override MAKE_DIR    := $(call s/\\s/\s/,$(MAKE_DIR))
endif
##
override PREFIX      := $(call s/\s/\\s/,$(PREFIX))
override PREFIX_MK   := $(call s/\s/\\s/,$(PREFIX_MK))
override LICENSE_TXT := $(call s/\s/\\s/,$(LICENSE_TXT))
override MAKEFILE    := $(call s/\s/\\s/,$(MAKEFILE))
override MAKE_DIR    := $(call s/\s/\\s/,$(MAKE_DIR))
##
override define _INCLUDE_GUARD_BEGIN :=
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS_PREFIX_MK_INCLUDED
define _MAKE_SCRIPTS_PREFIX_MK_INCLUDED :=
$$(lastword $$(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
endef
override define _INCLUDE_GUARD_END :=
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS_PREFIX_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
endef
override define _ERROR :=
define _MESSAGE :=
Scripts directory not found: $$(PREFIX)

Either run (some variation of) the

  git submodule update --init [--force] [--recursive] [--remote]

command or manually replace every occurance of the '$$(PREFIX)'
directory in the '$$(_MAKE_SCRIPTS_PREFIX_MK_INCLUDED)' makefile with
'<prefix>' and re-run the initial

  $$(_makecmdargs)

command or simply re-run as either the

  $$(call _makecmdargs,PREFIX='<prefix>')

command or the

  PREFIX='<prefix>' $$(_makecmdargs)

command, where '<prefix>' is a path to the directory containing files from the
'https://bitbucket.org/Alexander-Shukaev/make-scripts.git' repository
endef
$$(error $$(_MESSAGE))
override undefine _MESSAGE
endef
# ----------------------------------------------------------------------------
# BUG:
#
# If 'override' is specified in the previous definition of variable '_ERROR'
# for variable '_MESSAGE', i.e.
#
#   override define _MESSAGE :=
#
# then (GNU) Make's parser chokes on the
#
#   $$(error $$(_MESSAGE))
#
# line for whatever reason with the following error message:
#
#   Makefile:178: *** missing separator.  Stop.
#
# This is obviously a bug in the (GNU) Make's parser which can be worked
# around with the following (ugly) kludge:
#
override define _ERROR :=
define _makecmdargs
$$(MAKE)$$(if
$$(MAKEFLAGS), -$$(MAKEFLAGS))$$(if
$$(-*-command-variables-*-), $$(-*-command-variables-*-))$$(if
$$1, $$1)$$(if
$$(MAKECMDGOALS), $$(MAKECMDGOALS))
endef
override $(_ERROR)
override undefine _makecmdargs
endef
# ----------------------------------------------------------------------------
override define PREFIX_MK_CONTENT :=
$(_INCLUDE_GUARD_BEGIN)
##
override PREFIX ?= $(PREFIX)
ifneq "$$(PREFIX)" "$$(firstword $$(PREFIX))"
$$(error $$(PREFIX): Prefix not allowed: contains whitespace)
endif
ifeq "$$(wildcard $$(PREFIX)*)" ""
override $(_ERROR)
endif
##
$(_INCLUDE_GUARD_END)
endef
export PREFIX_MK_CONTENT
override undefine _INCLUDE_GUARD_BEGIN
override undefine _INCLUDE_GUARD_END
override undefine _ERROR
##
override define MAKEFILE_CONTENT :=
### Prefix {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
ifeq "$$(wildcard PREFIX.mk)" ""
$$(error PREFIX.mk: No such file or directory)
else
include PREFIX.mk
endif
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Prefix
##
### Header {{{
##  ==========================================================================
### Variables {{{
##  ==========================================================================
# TODO: ...
##  ==========================================================================
##  }}} Variables
##
### Includes {{{
##  ==========================================================================
include $$(PREFIX)header.mk
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Header
##
### Body {{{
##  ==========================================================================
### Variables {{{
##  ==========================================================================
# TODO: ...
##  ==========================================================================
##  }}} Variables
##
### Includes {{{
##  ==========================================================================
# TODO: ...
include $$(PREFIX)cmake.mk
include $$(PREFIX)git.mk
include $$(PREFIX)make.mk
include $$(PREFIX)python.mk
include $$(PREFIX)venv.mk
##  ==========================================================================
##  }}} Includes
##
### Variables {{{
##  ==========================================================================
# TODO: ...
# Default
override .DEFAULT_GOAL := all
##  ==========================================================================
##  }}} Variables
##
### Targets {{{
##  ==========================================================================
### All {{{
##  ==========================================================================
# TODO: ...
.PHONY: all
all: cmake git make python venv
##  ==========================================================================
##  }}} All
##  ==========================================================================
##  }}} Targets
##  ==========================================================================
##  }}} Body
##
### Footer {{{
##  ==========================================================================
### Variables {{{
##  ==========================================================================
# TODO: ...
##  ==========================================================================
##  }}} Variables
##
### Includes {{{
##  ==========================================================================
include $$(PREFIX)footer.mk
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Footer
endef
export MAKEFILE_CONTENT
##
override define MAKE_GOALS :=
  all
    cmake                         !cmake
      cmake.all                     !cmake.all
        cmake.build                   !cmake.build
    git
      git.all
        git.attributes
        git.config
        git.ignore
        git.hooks
        git.submodules
    make
      make.all
        make.scripts
    python
      python.all
        python.pytest
        python.setup
    venv                          !venv
      venv.all                      !venv.all
        venv.editable                 !venv.editable

  cmake                         !cmake
  cmake.all                     !cmake.all
  cmake.CMakeLists.txt
  cmake.configure               !cmake.configure
                                   cmake.clean.configure
                                   cmake.configure
  cmake.Debug                   !cmake.Debug
    cmake.configure               !cmake.configure
                                   cmake.Debug
  cmake.Release                 !cmake.Release
    cmake.configure               !cmake.configure
                                   cmake.Release
  cmake.RelWithDebInfo          !cmake.RelWithDebInfo
    cmake.configure               !cmake.configure
                                   cmake.RelWithDebInfo
  cmake.MinSizeRel              !cmake.MinSizeRel
    cmake.configure               !cmake.configure
                                   cmake.MinSizeRel
  cmake.build                   !cmake.build
    cmake.Debug                   !cmake.Debug
    cmake.Release                 !cmake.Release
    cmake.RelWithDebInfo          !cmake.RelWithDebInfo
    cmake.MinSizeRel              !cmake.MinSizeRel
  cmake.builder-help            !cmake.builder-help
    cmake.configure               !cmake.configure
                                   cmake.builder-help
  cmake.builder-clang-format    !cmake.builder-clang-format
    cmake.configure               !cmake.configure
                                   cmake.builder-clang-format
  cmake.builder-test            !cmake.builder-test
    cmake.configure               !cmake.configure
                                   cmake.builder-test
  cmake.builder-check           !cmake.builder-check
    cmake.configure               !cmake.configure
                                   cmake.builder-check
  cmake.builder-memcheck        !cmake.builder-memcheck
    cmake.configure               !cmake.configure
                                   cmake.builder-memcheck
  cmake.builder-valgrind        !cmake.builder-valgrind
    cmake.configure               !cmake.configure
                                   cmake.builder-valgrind
  cmake.builder-debugger        !cmake.builder-debugger
    cmake.configure               !cmake.configure
                                   cmake.builder-debugger
  cmake.builder.help            !cmake.builder.help
    cmake.configure               !cmake.configure
                                   cmake.builder.help
  cmake.builder.clang-format    !cmake.builder.clang-format
    cmake.configure               !cmake.configure
                                   cmake.builder.clang-format
  cmake.builder.test            !cmake.builder.test
    cmake.configure               !cmake.configure
                                   cmake.builder.test
  cmake.builder.check           !cmake.builder.check
    cmake.configure               !cmake.configure
                                   cmake.builder.check
  cmake.builder.memcheck        !cmake.builder.memcheck
    cmake.configure               !cmake.configure
                                   cmake.builder.memcheck
  cmake.builder.valgrind        !cmake.builder.valgrind
    cmake.configure               !cmake.configure
                                   cmake.builder.valgrind
  cmake.builder.debugger        !cmake.builder.debugger
    cmake.configure               !cmake.configure
                                   cmake.builder.debugger
  cmake.b-help                  !cmake.b-help
    cmake.configure               !cmake.configure
                                   cmake.b-help
  cmake.b-clang-format          !cmake.b-clang-format
    cmake.configure               !cmake.configure
                                   cmake.b-clang-format
  cmake.b-test                  !cmake.b-test
    cmake.configure               !cmake.configure
                                   cmake.b-test
  cmake.b-check                 !cmake.b-check
    cmake.configure               !cmake.configure
                                   cmake.b-check
  cmake.b-memcheck              !cmake.b-memcheck
    cmake.configure               !cmake.configure
                                   cmake.b-memcheck
  cmake.b-valgrind              !cmake.b-valgrind
    cmake.configure               !cmake.configure
                                   cmake.b-valgrind
  cmake.b-debugger              !cmake.b-debugger
    cmake.configure               !cmake.configure
                                   cmake.b-debugger
  cmake.b.help                  !cmake.b.help
    cmake.configure               !cmake.configure
                                   cmake.b.help
  cmake.b.clang-format          !cmake.b.clang-format
    cmake.configure               !cmake.configure
                                   cmake.b.clang-format
  cmake.b.test                  !cmake.b.test
    cmake.configure               !cmake.configure
                                   cmake.b.test
  cmake.b.check                 !cmake.b.check
    cmake.configure               !cmake.configure
                                   cmake.b.check
  cmake.b.memcheck              !cmake.b.memcheck
    cmake.configure               !cmake.configure
                                   cmake.b.memcheck
  cmake.b.valgrind              !cmake.b.valgrind
    cmake.configure               !cmake.configure
                                   cmake.b.valgrind
  cmake.b.debugger              !cmake.b.debugger
    cmake.configure               !cmake.configure
                                   cmake.b.debugger
  cmake.install                 !cmake.install
  cmake.install-strip           !cmake.install-strip
  cmake.install-local           !cmake.install-local
  cmake.install.strip           !cmake.install.strip
  cmake.install.local           !cmake.install.local
  cmake.installer-install       !cmake.installer-install
    cmake.build                   !cmake.build
                                   cmake.installer-install
  cmake.installer.install       !cmake.installer.install
    cmake.build                   !cmake.build
                                   cmake.installer.install
  cmake.i-install               !cmake.i-install
    cmake.build                   !cmake.build
                                   cmake.i-install
  cmake.i.install               !cmake.i.install
    cmake.build                   !cmake.build
                                   cmake.i.install
  cmake.uninstall
    cmake.clean.install
  cmake.clean
    cmake.clean.build
  cmake.clean.all
    cmake.clean.configure
    cmake.clean.build
    cmake.clean.install
  cmake.clean.configure
  cmake.clean.build
  cmake.clean.install
  cmake.command
  cmake.version

  CMakeLists.txt
  configure                     !configure
                                   clean.configure
                                   configure
  Debug                         !Debug
    configure                     !configure
                                   Debug
  Release                       !Release
    configure                     !configure
                                   Release
  RelWithDebInfo                !RelWithDebInfo
    configure                     !configure
                                   RelWithDebInfo
  MinSizeRel                    !MinSizeRel
    configure                     !configure
                                   MinSizeRel
  build                         !build
    Debug                         !Debug
    Release                       !Release
    RelWithDebInfo                !RelWithDebInfo
    MinSizeRel                    !MinSizeRel
  builder-help                  !builder-help
    configure                     !configure
                                   builder-help
  builder-clang-format          !builder-clang-format
    configure                     !configure
                                   builder-clang-format
  builder-test                  !builder-test
    configure                     !configure
                                   builder-test
  builder-check                 !builder-check
    configure                     !configure
                                   builder-check
  builder-memcheck              !builder-memcheck
    configure                     !configure
                                   builder-memcheck
  builder-valgrind              !builder-valgrind
    configure                     !configure
                                   builder-valgrind
  builder-debugger              !builder-debugger
    configure                     !configure
                                   builder-debugger
  builder.help                  !builder.help
    configure                     !configure
                                   builder.help
  builder.clang-format          !builder.clang-format
    configure                     !configure
                                   builder.clang-format
  builder.test                  !builder.test
    configure                     !configure
                                   builder.test
  builder.check                 !builder.check
    configure                     !configure
                                   builder.check
  builder.memcheck              !builder.memcheck
    configure                     !configure
                                   builder.memcheck
  builder.valgrind              !builder.valgrind
    configure                     !configure
                                   builder.valgrind
  builder.debugger              !builder.debugger
    configure                     !configure
                                   builder.debugger
  b-help                        !b-help
    configure                     !configure
                                   b-help
  b-clang-format                !b-clang-format
    configure                     !configure
                                   b-clang-format
  b-test                        !b-test
    configure                     !configure
                                   b-test
  b-check                       !b-check
    configure                     !configure
                                   b-check
  b-memcheck                    !b-memcheck
    configure                     !configure
                                   b-memcheck
  b-valgrind                    !b-valgrind
    configure                     !configure
                                   b-valgrind
  b-debugger                    !b-debugger
    configure                     !configure
                                   b-debugger
  b.help                        !b.help
    configure                     !configure
                                   b.help
  b.clang-format                !b.clang-format
    configure                     !configure
                                   b.clang-format
  b.test                        !b.test
    configure                     !configure
                                   b.test
  b.check                       !b.check
    configure                     !configure
                                   b.check
  b.memcheck                    !b.memcheck
    configure                     !configure
                                   b.memcheck
  b.valgrind                    !b.valgrind
    configure                     !configure
                                   b.valgrind
  b.debugger                    !b.debugger
    configure                     !configure
                                   b.debugger
  install                       !install
  install-strip                 !install-strip
  install-local                 !install-local
  install.strip                 !install.strip
  install.local                 !install.local
  installer-install             !installer-install
    build                         !build
                                   installer-install
  installer.install             !installer.install
    build                         !build
                                   installer.install
  i-install                     !i-install
    build                         !build
                                   i-install
  i.install                     !i.install
    build                         !build
                                   i.install
  uninstall
    clean.install
  clean
    clean.build
  clean.all
    clean.configure
    clean.build
    clean.install
  clean.configure
  clean.build
  clean.install
  command
  version

  git
  git.all
  git.attributes
  git.config
  git.ignore
  git.hooks
    git.locks
    git.update.hooks
  git.hooks.all
  git.hooks.install
  git.submodules
    git.locks
    git.update.submodules
  git.locks
  git.update
    git.update.hooks
    git.update.submodules
  git.update.hooks
  git.update.submodules
  git.command
  git.version

  make
  make.all
  make.help
  make.scripts
    make.update.scripts
  make.scripts.all
  make.scripts.help
  make.update
    make.update.scripts
  make.update.scripts
  make.command
  make.version

  python
  python.all
  python.pytest
    python.pytest.ini
  python.pytest.ini
  python.setup
    python.manifest.in
    python.setup.cfg
    python.setup.py
  python.manifest.in
  python.setup.cfg
  python.setup.py
  python.clean
    python.clean.all
  python.clean.all
    python.clean.pytest
    python.clean.setup
  python.clean.pytest
    python.clean.pytest.ini
  python.clean.pytest.ini
  python.clean.setup
    python.clean.manifest.in
    python.clean.setup.cfg
    python.clean.setup.py
  python.clean.manifest.in
  python.clean.setup.cfg
  python.clean.setup.py
  python.command
  python.version

  venv                          !venv
  venv.all                      !venv.all
  venv.setup                    !venv.setup
    venv.freeze.install           !venv.files
    venv.requirements              venv.setup
    python.setup
  venv.bdist                    !venv.bdist
    venv.setup                    !venv.setup
                                   venv.clean.bdist
                                   venv.bdist
  venv.sdist                    !venv.sdist
    venv.setup                    !venv.setup
                                   venv.clean.sdist
                                   venv.sdist
  venv.dist                     !venv.dist
    venv.bdist                    !venv.setup
    venv.sdist                     venv.clean.dist
                                   venv.dist
  venv.build                    !venv.build
    venv.setup                    !venv.setup
                                   venv.clean.build
                                   venv.build
  venv.develop                  !venv.develop
    venv.setup                    !venv.setup
                                   venv.clean.develop
                                   venv.develop
  venv.install                  !venv.install
    venv.setup                    !venv.setup
                                   venv.clean.install
                                   venv.install
  venv.test                     !venv.test
    venv.setup                    !venv.setup
                                   venv.test
  venv.activate                 !venv.activate
    venv.shell                    !venv.shell
  venv.ensurepip                !venv.ensurepip
    venv.files                    !venv.files
                                   venv.ensurepip
  venv.editable                 !venv.editable
    venv.freeze.install           !venv.files
    venv.editable.install          venv.editable
  venv.editable.install         !venv.editable.install
    venv.files                    !venv.files
                                   venv.editable.install
  venv.editable.uninstall
    venv.uninstall.
    venv.clean.editable
  venv.install-pip              !venv.install-pip
    venv.files                    !venv.files
                                  !venv.install-pip
  venv.install-pipreqs          !venv.install-pipreqs
    venv.files                    !venv.files
                                  !venv.install-pipreqs
  venv.install-setuptools       !venv.install-setuptools
    venv.files                    !venv.files
                                  !venv.install-setuptools
  venv.install-setuptools-scm   !venv.install-setuptools-scm
    venv.files                    !venv.files
                                  !venv.install-setuptools-scm
  venv.install-wheel            !venv.install-wheel
    venv.files                    !venv.files
                                  !venv.install-wheel
  venv.install-pytest           !venv.install-pytest
    venv.files                    !venv.files
                                  !venv.install-pytest
  venv.install-pytest-cov       !venv.install-pytest-cov
    venv.files                    !venv.files
                                  !venv.install-pytest-cov
  venv.install-pytest-flake8    !venv.install-pytest-flake8
    venv.files                    !venv.files
                                  !venv.install-pytest-flake8
  venv.install-pytest-pylint    !venv.install-pytest-pylint
    venv.files                    !venv.files
                                  !venv.install-pytest-pylint
  venv.install-pytest-raises    !venv.install-pytest-raises
    venv.files                    !venv.files
                                  !venv.install-pytest-raises
  venv.install-pytest-runner    !venv.install-pytest-runner
    venv.files                    !venv.files
                                  !venv.install-pytest-runner
  venv.install-pytest-timeout   !venv.install-pytest-timeout
    venv.files                    !venv.files
                                  !venv.install-pytest-timeout
  venv.install.                 !venv.install.
    venv.setup                    !venv.setup
                                   venv.install.
  venv.install.pip              !venv.install.pip
    venv.files                    !venv.files
                                  !venv.install.pip
  venv.install.pipreqs          !venv.install.pipreqs
    venv.files                    !venv.files
                                  !venv.install.pipreqs
  venv.install.setuptools       !venv.install.setuptools
    venv.files                    !venv.files
                                  !venv.install.setuptools
  venv.install.setuptools-scm   !venv.install.setuptools-scm
    venv.files                    !venv.files
                                  !venv.install.setuptools-scm
  venv.install.wheel            !venv.install.wheel
    venv.files                    !venv.files
                                  !venv.install.wheel
  venv.install.pytest           !venv.install.pytest
    venv.files                    !venv.files
                                  !venv.install.pytest
  venv.install.pytest-cov       !venv.install.pytest-cov
    venv.files                    !venv.files
                                  !venv.install.pytest-cov
  venv.install.pytest-flake8    !venv.install.pytest-flake8
    venv.files                    !venv.files
                                  !venv.install.pytest-flake8
  venv.install.pytest-pylint    !venv.install.pytest-pylint
    venv.files                    !venv.files
                                  !venv.install.pytest-pylint
  venv.install.pytest-raises    !venv.install.pytest-raises
    venv.files                    !venv.files
                                  !venv.install.pytest-raises
  venv.install.pytest-runner    !venv.install.pytest-runner
    venv.files                    !venv.files
                                  !venv.install.pytest-runner
  venv.install.pytest-timeout   !venv.install.pytest-timeout
    venv.files                    !venv.files
                                  !venv.install.pytest-timeout
  venv.upgrade-pip              !venv.upgrade-pip
    venv.files                    !venv.files
                                  !venv.upgrade-pip
  venv.upgrade-pipreqs          !venv.upgrade-pipreqs
    venv.files                    !venv.files
                                  !venv.upgrade-pipreqs
  venv.upgrade-setuptools       !venv.upgrade-setuptools
    venv.files                    !venv.files
                                  !venv.upgrade-setuptools
  venv.upgrade-setuptools-scm   !venv.upgrade-setuptools-scm
    venv.files                    !venv.files
                                  !venv.upgrade-setuptools-scm
  venv.upgrade-wheel            !venv.upgrade-wheel
    venv.files                    !venv.files
                                  !venv.upgrade-wheel
  venv.upgrade-pytest           !venv.upgrade-pytest
    venv.files                    !venv.files
                                  !venv.upgrade-pytest
  venv.upgrade-pytest-cov       !venv.upgrade-pytest-cov
    venv.files                    !venv.files
                                  !venv.upgrade-pytest-cov
  venv.upgrade-pytest-flake8    !venv.upgrade-pytest-flake8
    venv.files                    !venv.files
                                  !venv.upgrade-pytest-flake8
  venv.upgrade-pytest-pylint    !venv.upgrade-pytest-pylint
    venv.files                    !venv.files
                                  !venv.upgrade-pytest-pylint
  venv.upgrade-pytest-raises    !venv.upgrade-pytest-raises
    venv.files                    !venv.files
                                  !venv.upgrade-pytest-raises
  venv.upgrade-pytest-runner    !venv.upgrade-pytest-runner
    venv.files                    !venv.files
                                  !venv.upgrade-pytest-runner
  venv.upgrade-pytest-timeout   !venv.upgrade-pytest-timeout
    venv.files                    !venv.files
                                  !venv.upgrade-pytest-timeout
  venv.upgrade                  !venv.upgrade
    venv.upgrade.                 !venv.upgrade.
  venv.upgrade.                 !venv.upgrade.
    venv.setup                    !venv.setup
                                   venv.upgrade.
  venv.upgrade.pip              !venv.upgrade.pip
    venv.files                    !venv.files
                                  !venv.upgrade.pip
  venv.upgrade.pipreqs          !venv.upgrade.pipreqs
    venv.files                    !venv.files
                                  !venv.upgrade.pipreqs
  venv.upgrade.setuptools       !venv.upgrade.setuptools
    venv.files                    !venv.files
                                  !venv.upgrade.setuptools
  venv.upgrade.setuptools-scm   !venv.upgrade.setuptools-scm
    venv.files                    !venv.files
                                  !venv.upgrade.setuptools-scm
  venv.upgrade.wheel            !venv.upgrade.wheel
    venv.files                    !venv.files
                                  !venv.upgrade.wheel
  venv.upgrade.pytest           !venv.upgrade.pytest
    venv.files                    !venv.files
                                  !venv.upgrade.pytest
  venv.upgrade.pytest-cov       !venv.upgrade.pytest-cov
    venv.files                    !venv.files
                                  !venv.upgrade.pytest-cov
  venv.upgrade.pytest-flake8    !venv.upgrade.pytest-flake8
    venv.files                    !venv.files
                                  !venv.upgrade.pytest-flake8
  venv.upgrade.pytest-pylint    !venv.upgrade.pytest-pylint
    venv.files                    !venv.files
                                  !venv.upgrade.pytest-pylint
  venv.upgrade.pytest-raises    !venv.upgrade.pytest-raises
    venv.files                    !venv.files
                                  !venv.upgrade.pytest-raises
  venv.upgrade.pytest-runner    !venv.upgrade.pytest-runner
    venv.files                    !venv.files
                                  !venv.upgrade.pytest-runner
  venv.upgrade.pytest-timeout   !venv.upgrade.pytest-timeout
    venv.files                    !venv.files
                                  !venv.upgrade.pytest-timeout
  venv.uninstall
    venv.uninstall.
    venv.clean.install
  venv.uninstall-pip
    venv.files
  venv.uninstall-pipreqs
    venv.files
  venv.uninstall-setuptools
    venv.files
  venv.uninstall-setuptools-scm
    venv.files
  venv.uninstall-wheel
    venv.files
  venv.uninstall-pytest
    venv.files
  venv.uninstall-pytest-cov
    venv.files
  venv.uninstall-pytest-flake8
    venv.files
  venv.uninstall-pytest-pylint
    venv.files
  venv.uninstall-pytest-raises
    venv.files
  venv.uninstall-pytest-runner
    venv.files
  venv.uninstall-pytest-timeout
    venv.files
  venv.uninstall.
  venv.uninstall.pip
    venv.files
  venv.uninstall.pipreqs
    venv.files
  venv.uninstall.setuptools
    venv.files
  venv.uninstall.setuptools-scm
    venv.files
  venv.uninstall.wheel
    venv.files
  venv.uninstall.pytest
    venv.files
  venv.uninstall.pytest-cov
    venv.files
  venv.uninstall.pytest-flake8
    venv.files
  venv.uninstall.pytest-pylint
    venv.files
  venv.uninstall.pytest-raises
    venv.files
  venv.uninstall.pytest-runner
    venv.files
  venv.uninstall.pytest-timeout
    venv.files
  venv.freeze                   !venv.freeze
    venv.files                    !venv.files
                                  !venv.freeze
  venv.freeze.print
    venv.files
  venv.freeze.install           !venv.freeze.install
    venv.files                    !venv.files
                                  !venv.freeze.install
  venv.freeze.upgrade           !venv.freeze.upgrade
    venv.files                    !venv.files
                                  !venv.freeze.upgrade
  venv.freeze.uninstall
    venv.files
  venv.requirements             !venv.requirements
    venv.files                    !venv.files
                                  !venv.requirements
  venv.requirements.print
    venv.files
  venv.requirements.install     !venv.requirements.install
    venv.requirements             !venv.requirements
                                  !venv.requirements.install
  venv.requirements.upgrade     !venv.requirements.upgrade
    venv.requirements             !venv.requirements
                                  !venv.requirements.upgrade
  venv.requirements.uninstall
    venv.requirements
  venv.shell                    !venv.shell
    venv.files                    !venv.files
                                  !venv.shell
  venv.pytest                   !venv.pytest
    venv.files                    !venv.files
    python.pytest                  venv.pytest
  venv.pytest-develop           !venv.pytest-develop
    venv.develop                  !venv.develop
    venv.pytest                    venv.pytest
  venv.pytest-editable          !venv.pytest-editable
    venv.editable                 !venv.editable
    venv.pytest                    venv.pytest
  venv.pytest-install           !venv.pytest-install
    venv.install                  !venv.install
    venv.pytest                    venv.pytest
  venv.pytest-install.          !venv.pytest-install.
    venv.install.                 !venv.install.
    venv.pytest                    venv.pytest
  venv.pytest-package           !venv.pytest-package
    venv.pytest.package           !venv.pytest.package
  venv.pytest.package           !venv.pytest.package
  venv.files                    !venv.files
                                   venv.clean.files
                                   venv.files
  venv.clean
    venv.clean.build
  venv.clean.all
    venv.clean.build
    venv.clean.editable
    venv.clean.dist
    venv.clean.files
  venv.clean.install
    venv.clean.build
  venv.clean.build
  venv.clean.develop
    venv.clean.editable
  venv.clean.editable
  venv.clean.bdist
    venv.clean.dist
  venv.clean.sdist
    venv.clean.dist
  venv.clean.dist
  venv.clean.files

  setup                         !setup
    freeze.install                !files
    requirements                   setup
    python.setup
  bdist                         !bdist
    setup                         !setup
                                   clean.bdist
                                   bdist
  sdist                         !sdist
    setup                         !setup
                                   clean.sdist
                                   sdist
  dist                          !dist
    bdist                         !setup
    sdist                          clean.dist
                                   dist
  build                         !build
    setup                         !setup
                                   clean.build
                                   build
  develop                       !develop
    setup                         !setup
                                   clean.develop
                                   develop
  install                       !install
    setup                         !setup
                                   clean.install
                                   install
  test                          !test
    setup                         !setup
                                   test
  activate                      !activate
    shell                         !shell
  ensurepip                     !ensurepip
    files                         !files
                                   ensurepip
  editable                      !editable
    freeze.install                !files
    editable.install               editable
  editable.install              !editable.install
    files                         !files
                                   editable.install
  editable.uninstall
    uninstall.
    clean.editable
  install-pip                   !install-pip
    files                         !files
                                  !install-pip
  install-pipreqs               !install-pipreqs
    files                         !files
                                  !install-pipreqs
  install-setuptools            !install-setuptools
    files                         !files
                                  !install-setuptools
  install-setuptools-scm        !install-setuptools-scm
    files                         !files
                                  !install-setuptools-scm
  install-wheel                 !install-wheel
    files                         !files
                                  !install-wheel
  install-pytest                !install-pytest
    files                         !files
                                  !install-pytest
  install-pytest-cov            !install-pytest-cov
    files                         !files
                                  !install-pytest-cov
  install-pytest-flake8         !install-pytest-flake8
    files                         !files
                                  !install-pytest-flake8
  install-pytest-pylint         !install-pytest-pylint
    files                         !files
                                  !install-pytest-pylint
  install-pytest-raises         !install-pytest-raises
    files                         !files
                                  !install-pytest-raises
  install-pytest-runner         !install-pytest-runner
    files                         !files
                                  !install-pytest-runner
  install-pytest-timeout        !install-pytest-timeout
    files                         !files
                                  !install-pytest-timeout
  install.                      !install.
    setup                         !setup
                                   install.
  install.pip                   !install.pip
    files                         !files
                                  !install.pip
  install.pipreqs               !install.pipreqs
    files                         !files
                                  !install.pipreqs
  install.setuptools            !install.setuptools
    files                         !files
                                  !install.setuptools
  install.setuptools-scm        !install.setuptools-scm
    files                         !files
                                  !install.setuptools-scm
  install.wheel                 !install.wheel
    files                         !files
                                  !install.wheel
  install.pytest                !install.pytest
    files                         !files
                                  !install.pytest
  install.pytest-cov            !install.pytest-cov
    files                         !files
                                  !install.pytest-cov
  install.pytest-flake8         !install.pytest-flake8
    files                         !files
                                  !install.pytest-flake8
  install.pytest-pylint         !install.pytest-pylint
    files                         !files
                                  !install.pytest-pylint
  install.pytest-raises         !install.pytest-raises
    files                         !files
                                  !install.pytest-raises
  install.pytest-runner         !install.pytest-runner
    files                         !files
                                  !install.pytest-runner
  install.pytest-timeout        !install.pytest-timeout
    files                         !files
                                  !install.pytest-timeout
  upgrade-pip                   !upgrade-pip
    files                         !files
                                  !upgrade-pip
  upgrade-pipreqs               !upgrade-pipreqs
    files                         !files
                                  !upgrade-pipreqs
  upgrade-setuptools            !upgrade-setuptools
    files                         !files
                                  !upgrade-setuptools
  upgrade-setuptools-scm        !upgrade-setuptools-scm
    files                         !files
                                  !upgrade-setuptools-scm
  upgrade-wheel                 !upgrade-wheel
    files                         !files
                                  !upgrade-wheel
  upgrade-pytest                !upgrade-pytest
    files                         !files
                                  !upgrade-pytest
  upgrade-pytest-cov            !upgrade-pytest-cov
    files                         !files
                                  !upgrade-pytest-cov
  upgrade-pytest-flake8         !upgrade-pytest-flake8
    files                         !files
                                  !upgrade-pytest-flake8
  upgrade-pytest-pylint         !upgrade-pytest-pylint
    files                         !files
                                  !upgrade-pytest-pylint
  upgrade-pytest-raises         !upgrade-pytest-raises
    files                         !files
                                  !upgrade-pytest-raises
  upgrade-pytest-runner         !upgrade-pytest-runner
    files                         !files
                                  !upgrade-pytest-runner
  upgrade-pytest-timeout        !upgrade-pytest-timeout
    files                         !files
                                  !upgrade-pytest-timeout
  upgrade                       !upgrade
    upgrade.                      !upgrade.
  upgrade.                      !upgrade.
    setup                         !setup
                                   upgrade.
  upgrade.pip                   !upgrade.pip
    files                         !files
                                  !upgrade.pip
  upgrade.pipreqs               !upgrade.pipreqs
    files                         !files
                                  !upgrade.pipreqs
  upgrade.setuptools            !upgrade.setuptools
    files                         !files
                                  !upgrade.setuptools
  upgrade.setuptools-scm        !upgrade.setuptools-scm
    files                         !files
                                  !upgrade.setuptools-scm
  upgrade.wheel                 !upgrade.wheel
    files                         !files
                                  !upgrade.wheel
  upgrade.pytest                !upgrade.pytest
    files                         !files
                                  !upgrade.pytest
  upgrade.pytest-cov            !upgrade.pytest-cov
    files                         !files
                                  !upgrade.pytest-cov
  upgrade.pytest-flake8         !upgrade.pytest-flake8
    files                         !files
                                  !upgrade.pytest-flake8
  upgrade.pytest-pylint         !upgrade.pytest-pylint
    files                         !files
                                  !upgrade.pytest-pylint
  upgrade.pytest-raises         !upgrade.pytest-raises
    files                         !files
                                  !upgrade.pytest-raises
  upgrade.pytest-runner         !upgrade.pytest-runner
    files                         !files
                                  !upgrade.pytest-runner
  upgrade.pytest-timeout        !upgrade.pytest-timeout
    files                         !files
                                  !upgrade.pytest-timeout
  uninstall
    uninstall.
    clean.install
  uninstall-pip
    files
  uninstall-pipreqs
    files
  uninstall-setuptools
    files
  uninstall-setuptools-scm
    files
  uninstall-wheel
    files
  uninstall-pytest
    files
  uninstall-pytest-cov
    files
  uninstall-pytest-flake8
    files
  uninstall-pytest-pylint
    files
  uninstall-pytest-raises
    files
  uninstall-pytest-runner
    files
  uninstall-pytest-timeout
    files
  uninstall.
  uninstall.pip
    files
  uninstall.pipreqs
    files
  uninstall.setuptools
    files
  uninstall.setuptools-scm
    files
  uninstall.wheel
    files
  uninstall.pytest
    files
  uninstall.pytest-cov
    files
  uninstall.pytest-flake8
    files
  uninstall.pytest-pylint
    files
  uninstall.pytest-raises
    files
  uninstall.pytest-runner
    files
  uninstall.pytest-timeout
    files
  freeze                        !freeze
    files                         !files
                                  !freeze
  freeze.print
    files
  freeze.install                !freeze.install
    files                         !files
                                  !freeze.install
  freeze.upgrade                !freeze.upgrade
    files                         !files
                                  !freeze.upgrade
  freeze.uninstall
    files
  requirements                  !requirements
    files                         !files
                                  !requirements
  requirements.print
    files
  requirements.install          !requirements.install
    requirements                  !requirements
                                  !requirements.install
  requirements.upgrade          !requirements.upgrade
    requirements                  !requirements
                                  !requirements.upgrade
  requirements.uninstall
    requirements
  shell                         !shell
    files                         !files
                                  !shell
  pytest                        !pytest
    files                         !files
    python.pytest                  pytest
  pytest-develop                !pytest-develop
    develop                       !develop
    pytest                         pytest
  pytest-editable               !pytest-editable
    editable                      !editable
    pytest                         pytest
  pytest-install                !pytest-install
    install                       !install
    pytest                         pytest
  pytest-install.               !pytest-install.
    install.                      !install.
    pytest                         pytest
  pytest-package                !pytest-package
    pytest.package                !pytest.package
  pytest.package                !pytest.package
  files                         !files
                                   clean.files
                                   files
  clean
    clean.build
  clean.all
    clean.build
    clean.editable
    clean.dist
    clean.files
  clean.install
    clean.build
  clean.build
  clean.develop
    clean.editable
  clean.editable
  clean.bdist
    clean.dist
  clean.sdist
    clean.dist
  clean.dist
  clean.files
endef
##
override define MAKE_HELP :=
$(HRULE80)
make: Goals:
$(MAKE_GOALS)
$(HRULE80)
endef
ifdef .MAKELEVEL0
ifdef VERBOSE
$(info $(MAKE_HELP))
endif
endif
##
override MAKE_GOALS := $(call uniq,$(call s/\n/\s/,$(MAKE_GOALS)))
override MAKE_LINKS := $(addprefix $(MAKE_DIR)/,$(MAKE_GOALS))
##
override define MAKE_LINKS_PREFIX :=
$(call s/\\c/\\s/,$(call relpath,$(__PWD__),$\
$(call s/\\s/\\c/,$(MAKE_DIR))))/$(PREFIX)
endef
##
override undefine __CURDIR__
override undefine __PWD__
##
### Make Scripts {{{
##  ==========================================================================
### All {{{
##  ==========================================================================
# Default
override .DEFAULT_GOAL := all
##
.PHONY: all
all: prefix.mk license.txt makefile .make
##  ==========================================================================
##  }}} All
##
### Prefix {{{
##  ==========================================================================
.PHONY: prefix.mk
prefix.mk: $(PREFIX_MK)
##  ==========================================================================
##  }}} Prefix
##
### License {{{
##  ==========================================================================
.PHONY: license.txt
license.txt: $(LICENSE_TXT)
##  ==========================================================================
##  }}} License
##
### Makefile {{{
##  ==========================================================================
# ----------------------------------------------------------------------------
.PHONY: makefile
makefile: $(MAKEFILE)
#
# BUG:
#
# Renaming (capitalizing) the 'makefile' goal to the 'Makefile' goal leads to
# unexpected behavior.  That is somehow the (resulting) 'Makefile' goal is
# always executed first regardless of whether it was even specified or not.
# ----------------------------------------------------------------------------
##  ==========================================================================
##  }}} Makefile
##
### Make Links {{{
##  ==========================================================================
.PHONY: .make
.make: $(MAKE_LINKS)
##  ==========================================================================
##  }}} Make Links
##
### Clean {{{
##  ==========================================================================
.PHONY: clean
clean: clean/all
##
.PHONY: clean.%
clean.%: clean/%
	@
##
.PHONY: clean/all
clean/all: clean/prefix.mk clean/license.txt clean/makefile clean/.make
##
.PHONY: clean/prefix.mk
clean/prefix.mk:
	@$(if $(VERBOSE),printf "make: Removing file '%s'\n" $(PREFIX_MK))
	@rm -f -- $(PREFIX_MK)
##
.PHONY: clean/license.txt
clean/license.txt:
	@$(if $(VERBOSE),printf "make: Removing file '%s'\n" $(LICENSE_TXT))
	@rm -f -- $(LICENSE_TXT)
##
.PHONY: clean/makefile
clean/makefile:
	@$(if $(VERBOSE),printf "make: Removing file '%s'\n" $(MAKEFILE))
	@rm -f -- $(MAKEFILE)
##
.PHONY: clean/.make
clean/.make:
	@$(if $(VERBOSE),printf "make: Removing directory '%s'\n" $(MAKE_DIR))
	@rm -f -R -- $(MAKE_DIR)
##  ==========================================================================
##  }}} Clean
##
### Help {{{
##  ==========================================================================
.PHONY: help
help: | .silent
	@$(info $(MAKE_HELP))
##  ==========================================================================
##  }}} Help
##
$(PREFIX_MK):
	@$(if $(VERBOSE),printf "make: Writing file '%s'\n" '$@')
	@echo "$${PREFIX_MK_CONTENT}" >| '$@'
##
$(LICENSE_TXT): LICENSE.txt
	@$(if $(VERBOSE),printf "make: Writing file '%s'\n" '$@')
	@cp -- $(__DIR__)/$< '$@'
##
$(MAKEFILE): | $(PREFIX_MK)
	@$(if $(VERBOSE),printf "make: Writing file '%s'\n" '$@')
	@echo "$${MAKEFILE_CONTENT}" >| '$@'
##
$(MAKE_LINKS): | make.sh $(MAKE_DIR)
	@$(if $(VERBOSE),printf "make: Creating symbolic link '%s' -> '%s'\n"\
	   '$@' $(MAKE_LINKS_PREFIX)$&)
	@ln -s -- $(MAKE_LINKS_PREFIX)$& '$@'
##
$(MAKE_DIR):
	@$(if $(VERBOSE),printf "make: Creating directory '%s'\n" '$@')
	@mkdir -p '$@'
##  ==========================================================================
##  }}} Make Scripts
##
### Includes {{{
##  ==========================================================================
include $(__R_DIR__)footer.mk
##  ==========================================================================
##  }}} Includes
endif
