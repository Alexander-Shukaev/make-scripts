### Preamble {{{
##  ==========================================================================
##        @file shortcut_goals.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-10-01 Monday 17:03:55 (+0200)
##  --------------------------------------------------------------------------
##     @created 2018-09-17 Monday 02:29:36 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS_VENV_SHORTCUT_GOALS_MK_INCLUDED
define _MAKE_SCRIPTS_VENV_SHORTCUT_GOALS_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##
ifdef _MAKE_SCRIPTS_CMAKE_SHORTCUT_GOALS_MK_INCLUDED
override define _MESSAGE :=
$(_MAKE_SCRIPTS_VENV_SHORTCUT_GOALS_MK_INCLUDED): Include not allowed:       \
conflicts with (already included) makefile                                   \
'$(_MAKE_SCRIPTS_CMAKE_SHORTCUT_GOALS_MK_INCLUDED)'
endef
$(error $(_MESSAGE))
override undefine _MESSAGE
endif
##
override __VENV_SHORTCUT_GOALS_R_FILE__ :=                                   \
  $(_MAKE_SCRIPTS_VENV_SHORTCUT_GOALS_MK_INCLUDED)
override __VENV_SHORTCUT_GOALS_R_DIR__  :=                                   \
  $(dir $(__VENV_SHORTCUT_GOALS_R_FILE__))
##
### Includes {{{
##  ==========================================================================
include $(__VENV_SHORTCUT_GOALS_R_DIR__)../.global.mk
include $(__VENV_SHORTCUT_GOALS_R_DIR__)../.verbose.mk
include $(__VENV_SHORTCUT_GOALS_R_DIR__)../venv.mk
##  ==========================================================================
##  }}} Includes
##
override __VENV_SHORTCUT_GOALS_FILE__ :=                                     \
  $(abspath $(__VENV_SHORTCUT_GOALS_R_FILE__))
override __VENV_SHORTCUT_GOALS_DIR__  :=                                     \
  $(abspath $(__VENV_SHORTCUT_GOALS_R_DIR__))
##
ifdef VERBOSE
$(info make: Reading makefile '$(__VENV_SHORTCUT_GOALS_FILE__)')
endif
##
override __VENV_SHORTCUT_GOALS_FILE__ :=                                     \
  $(call s/\s/\\s/,$(__VENV_SHORTCUT_GOALS_FILE__))
override __VENV_SHORTCUT_GOALS_DIR__  :=                                     \
  $(call s/\s/\\s/,$(__VENV_SHORTCUT_GOALS_DIR__))
##
override VENV_DEFINE_SHORTCUT_GOALS ?= Y
override VENV_DEFINE_SHORTCUT_GOALS :=                                       \
  $(call uppercase,$(strip $(VENV_DEFINE_SHORTCUT_GOALS)))
ifeq "$(VENV_DEFINE_SHORTCUT_GOALS)" "N"
override VENV_DEFINE_SHORTCUT_GOALS :=
endif
ifeq "$(VENV_DEFINE_SHORTCUT_GOALS)" "0"
override VENV_DEFINE_SHORTCUT_GOALS :=
endif
ifdef VENV_DEFINE_SHORTCUT_GOALS
override VENV_DEFINE_SHORTCUT_GOALS := 1
endif
##
ifdef VENV_DEFINE_SHORTCUT_GOALS
### Virtual Environment {{{
##  ==========================================================================
### Setup {{{
##  ==========================================================================
.PHONY: setup
setup: venv/setup
##
.PHONY: !setup
!setup: !venv/setup
##
### Built Distribution {{{
##  ==========================================================================
.PHONY: bdist
bdist: venv/bdist
##
.PHONY: !bdist
!bdist: !venv/bdist
##  ==========================================================================
##  }}} Built Distribution
##
### Source Distribution {{{
##  ==========================================================================
.PHONY: sdist
sdist: venv/sdist
##
.PHONY: !sdist
!sdist: !venv/sdist
##  ==========================================================================
##  }}} Source Distribution
##
### Distribution {{{
##  ==========================================================================
.PHONY: dist
dist: venv/dist
##
.PHONY: !dist
!dist: !venv/dist
##  ==========================================================================
##  }}} Distribution
##
### Build {{{
##  ==========================================================================
.PHONY: build
build: venv/build
##
.PHONY: !build
!build: !venv/build
##  ==========================================================================
##  }}} Build
##
### Develop {{{
##  ==========================================================================
.PHONY: develop
develop: venv/develop
##
.PHONY: !develop
!develop: !venv/develop
##  ==========================================================================
##  }}} Develop
##
### Test {{{
##  ==========================================================================
.PHONY: test
test: venv/test
##
.PHONY: !test
!test: !venv/test
##  ==========================================================================
##  }}} Test
##  ==========================================================================
##  }}} Setup
##
### Activate {{{
##  ==========================================================================
.PHONY: activate
activate: venv/activate
##
.PHONY: !activate
!activate: !venv/activate
##  ==========================================================================
##  }}} Activate
##
### Ensure PIP {{{
##  ==========================================================================
.PHONY: ensurepip
ensurepip: venv/ensurepip
##
.PHONY: !ensurepip
!ensurepip: !venv/ensurepip
##  ==========================================================================
##  }}} Ensure PIP
##
### Editable {{{
##  ==========================================================================
.PHONY: editable
editable: venv/editable
##
.PHONY: editable%
editable%: venv/editable%
	@
##
.PHONY: !editable
!editable: !venv/editable
##
.PHONY: !editable%
!editable%: !venv/editable%
	@
##  ==========================================================================
##  }}} Editable
##
### Install {{{
##  ==========================================================================
.PHONY: install
install: venv/install
##
.PHONY: install%
install%: venv/install%
	@
##
.PHONY: !install
!install: !venv/install
##
.PHONY: !install%
!install%: !venv/install%
	@
##  ==========================================================================
##  }}} Install
##
### Upgrade {{{
##  ==========================================================================
.PHONY: upgrade
upgrade: venv/upgrade
##
.PHONY: upgrade%
upgrade%: venv/upgrade%
	@
##
.PHONY: !upgrade
!upgrade: !venv/upgrade
##
.PHONY: !upgrade%
!upgrade%: !venv/upgrade%
	@
##  ==========================================================================
##  }}} Upgrade
##
### Uninstall {{{
##  ==========================================================================
.PHONY: uninstall
uninstall: venv/uninstall
##
.PHONY: uninstall%
uninstall%: venv/uninstall%
	@
##  ==========================================================================
##  }}} Uninstall
##
### Freeze {{{
##  ==========================================================================
.PHONY: freeze
freeze: venv/freeze
##
.PHONY: freeze%
freeze%: venv/freeze%
	@
##
.PHONY: !freeze
!freeze: !venv/freeze
##
.PHONY: !freeze%
!freeze%: !venv/freeze%
	@
##  ==========================================================================
##  }}} Freeze
##
### Requirements {{{
##  ==========================================================================
.PHONY: requirements
requirements: venv/requirements
##
.PHONY: requirements%
requirements%: venv/requirements%
	@
##
.PHONY: !requirements
!requirements: !venv/requirements
##
.PHONY: !requirements%
!requirements%: !venv/requirements%
	@
##  ==========================================================================
##  }}} Requirements
##
### Shell {{{
##  ==========================================================================
.PHONY: shell
shell: venv/shell
##
.PHONY: !shell
!shell: !venv/shell
##  ==========================================================================
##  }}} Shell
##
### Pytest {{{
##  ==========================================================================
.PHONY: pytest
pytest: venv/pytest
##
.PHONY: pytest%
pytest%: venv/pytest%
	@
##
.PHONY: !pytest
!pytest: !venv/pytest
##
.PHONY: !pytest%
!pytest%: !venv/pytest%
	@
##  ==========================================================================
##  }}} Pytest
##
### Files {{{
##  ==========================================================================
.PHONY: files
files: venv/files
##
.PHONY: !files
!files: !venv/files
##  ==========================================================================
##  }}} Files
##
### Clean {{{
##  ==========================================================================
.PHONY: clean
clean: venv/clean
##
.PHONY: clean%
clean%: venv/clean%
	@
##  ==========================================================================
##  }}} Clean
##  ==========================================================================
##  }}} Virtual Environment
endif
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS_VENV_SHORTCUT_GOALS_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
