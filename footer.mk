### Preamble {{{
##  ==========================================================================
##        @file footer.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-11-21 Wednesday 01:11:23 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-12-15 Tuesday 22:33:06 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS_FOOTER_MK_INCLUDED
define _MAKE_SCRIPTS_FOOTER_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##
override __FOOTER_R_FILE__ := $(_MAKE_SCRIPTS_FOOTER_MK_INCLUDED)
override __FOOTER_R_DIR__  := $(dir $(__FOOTER_R_FILE__))
##
### Includes {{{
##  ==========================================================================
include $(__FOOTER_R_DIR__)header.mk
##
include $(__FOOTER_R_DIR__).global.mk
include $(__FOOTER_R_DIR__).makelevel.mk
include $(__FOOTER_R_DIR__).verbose.mk
include $(__FOOTER_R_DIR__).silent.mk
include $(__FOOTER_R_DIR__).debug.mk
include $(__FOOTER_R_DIR__).trace.mk
include $(__FOOTER_R_DIR__).shell.mk
include $(__FOOTER_R_DIR__).expand.mk
include $(__FOOTER_R_DIR__).value.mk
##  ==========================================================================
##  }}} Includes
##
override __FOOTER_FILE__ := $(abspath $(__FOOTER_R_FILE__))
override __FOOTER_DIR__  := $(abspath $(__FOOTER_R_DIR__))
##
ifdef VERBOSE
$(info make: Reading makefile '$(__FOOTER_FILE__)')
endif
##
override __FOOTER_FILE__ := $(call s/\s/\\s/,$(__FOOTER_FILE__))
override __FOOTER_DIR__  := $(call s/\s/\\s/,$(__FOOTER_DIR__))
##
ifdef EXPAND
$(info $(.expand))
else
ifdef VERBOSE
ifdef .EXPAND_VARIABLES
$(info $(.expand))
endif
endif
endif
##
ifdef VALUE
$(info $(.value))
else
ifdef VERBOSE
ifdef .VALUE_VARIABLES
$(info $(.value))
endif
endif
endif
##
$(call uniq,$(MAKEFILE_LIST)):
	@
##
%:: %,v
%:: RCS/%
%:: RCS/%,v
%:: SCCS/s.%
%:: s.%
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS_FOOTER_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
