### Preamble {{{
##  ==========================================================================
##        @file header.mk
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-11-21 Wednesday 01:10:58 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-12-15 Tuesday 22:33:06 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS_HEADER_MK_INCLUDED
define _MAKE_SCRIPTS_HEADER_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
ifeq "$(lastword $(MAKEFILE_LIST))" "$(firstword $(MAKEFILE_LIST))"
$(error $(MAKEFILE_LIST): Read not allowed: read as first makefile)
endif
##
override _MAKEFILE_LIST :=                                                   \
  $(filter-out $(_MAKE_SCRIPTS_PREFIX_MK_INCLUDED)                           \
               $(_MAKE_SCRIPTS_HEADER_MK_INCLUDED),$(MAKEFILE_LIST))
##
override __HEADER_R_FILE__ := $(_MAKE_SCRIPTS_HEADER_MK_INCLUDED)
override __HEADER_R_DIR__  := $(dir $(__HEADER_R_FILE__))
##
### Includes {{{
##  ==========================================================================
include $(__HEADER_R_DIR__).global.mk
include $(__HEADER_R_DIR__).makelevel.mk
include $(__HEADER_R_DIR__).verbose.mk
include $(__HEADER_R_DIR__).silent.mk
include $(__HEADER_R_DIR__).debug.mk
include $(__HEADER_R_DIR__).trace.mk
include $(__HEADER_R_DIR__).shell.mk
include $(__HEADER_R_DIR__).expand.mk
include $(__HEADER_R_DIR__).value.mk
##  ==========================================================================
##  }}} Includes
##
override __R_FILE__ := $(lastword $(_MAKEFILE_LIST))
override __R_DIR__  := $(dir      $(__R_FILE__))
##
ifneq "$(__R_FILE__)" "$(firstword $(_MAKEFILE_LIST))"
$(error $(__R_FILE__): Include not allowed)
endif
##
override undefine _MAKEFILE_LIST
##
override __FILE__ := $(abspath $(__R_FILE__))
override __DIR__  := $(abspath $(__R_DIR__))
##
ifdef VERBOSE
$(info make: Reading makefile '$(__FILE__)')
endif
##
override __FILE__ := $(call s/\s/\\s/,$(__FILE__))
override __DIR__  := $(call s/\s/\\s/,$(__DIR__))
##
override __HEADER_FILE__ := $(abspath $(__HEADER_R_FILE__))
override __HEADER_DIR__  := $(abspath $(__HEADER_R_DIR__))
##
ifdef VERBOSE
$(info make: Reading makefile '$(__HEADER_FILE__)')
endif
##
override __HEADER_FILE__ := $(call s/\s/\\s/,$(__HEADER_FILE__))
override __HEADER_DIR__  := $(call s/\s/\\s/,$(__HEADER_DIR__))
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS_HEADER_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
